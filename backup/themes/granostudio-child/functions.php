<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */


/**
 * Enqueues scripts and styles.
 */

 // Um custom post type chamado 'e-books'
	function wptutsplus_create_post_type() {

      // Set UI labels for Custom Post Type
      	$labels = array(
      		'name'                => _x( 'Videos', 'Post Type General Name', 'twentythirteen' ),
      		'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'twentythirteen' ),
      		'menu_name'           => __( 'Videos', 'twentythirteen' ),
      		'parent_item_colon'   => __( 'Parent Video', 'twentythirteen' ),
      		'all_items'           => __( 'Todos os videos', 'twentythirteen' ),
      		'view_item'           => __( 'View Video', 'twentythirteen' ),
      		'add_new_item'        => __( 'Adicionar novo video', 'twentythirteen' ),
      		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
      		'edit_item'           => __( 'Editar Video', 'twentythirteen' ),
      		'update_item'         => __( 'Update Video', 'twentythirteen' ),
      		'search_items'        => __( 'Procurar', 'twentythirteen' ),
      		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
      		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
      	);

      // Set other options for Custom Post Type

      	$args = array(
      		'label'               => __( 'videos', 'twentythirteen' ),
      		'description'         => __( 'Videos', 'twentythirteen' ),
      		'labels'              => $labels,
      		// Features this CPT supports in Post Editor
      		'supports'            => array( 'title', 'editor', 'thumbnail' ),
      		// You can associate this CPT with a taxonomy or custom taxonomy.
      	// 	'taxonomies'          => array( 'genres' ),
      		/* A hierarchical CPT is like Pages and can have
      		* Parent and child items. A non-hierarchical CPT
      		* is like Posts.
      		*/
      		'hierarchical'        => false,
      		'public'              => true,
      		'show_ui'             => true,
      		'show_in_menu'        => true,
      		'show_in_nav_menus'   => true,
      		'show_in_admin_bar'   => true,
      		'menu_position'       => 1,
      		'can_export'          => true,
      		'has_archive'         => true,
      		'exclude_from_search' => false,
      		'publicly_queryable'  => true,
      		'capability_type'     => 'page',
      	);

      	// Registering your Custom Post Type
      	register_post_type( 'videos', $args );


				$labels = array(
			        'name' => __( 'E-Books' ),
			        'singular_name' => __( 'e-book' ),
			        'add_new' => __( 'Novo E-book' ),
			        'add_new_item' => __( 'Adicionar Novo E-Book' ),
			        'edit_item' => __( 'Editar E-Book' ),
			        'new_item' => __( 'Novo E-Book' ),
			        'view_item' => __( 'View E-Book' ),
			        'search_items' => __( 'Procurar E-Book' ),
			        'not_found' =>  __( 'E-Books não encontrados' ),
			        'not_found_in_trash' => __( 'No e-books found in Trash' ),
			    );
			    $args = array(
			        'labels' => $labels,
			        'has_archive' => true,
			        'public' => true,
			        'hierarchical' => false,
			        'supports' => array(
			            'title',
			            'editor',
			            'excerpt' => false,
			            'custom-fields' => false,
			            'thumbnail',
			            'page-attributes' => false,
			        ),
			        'taxonomies' => array( 'post_tag', 'category'),
			    );
			    register_post_type( 'e-book', $args );



					$labels = array(
				        'name' => __( 'Destaques' ),
				        'singular_name' => __( 'destaque' ),
				        'add_new' => __( 'Novo Post Destaque' ),
				        'add_new_item' => __( 'Adicionar Novo Post Destaque' ),
				        'edit_item' => __( 'Editar Post Destaque' ),
				        'new_item' => __( 'Novo Post Destaque' ),
				        'view_item' => __( 'View Post Destaque' ),
				        'search_items' => __( 'Procurar Post Destaque' ),
				        'not_found' =>  __( 'Post Destaque não encontrados' ),
				        'not_found_in_trash' => __( 'No Post Destaque found in Trash' ),
				    );
				    $args = array(
				        'labels' => $labels,
				        'has_archive' => true,
				        'public' => true,
				        'hierarchical' => false,
				        'supports' => array(
				            'title',
				            'editor',
				            'excerpt' => false,
				            'custom-fields' => false,
				            'thumbnail',
				            'page-attributes' => false,
				        ),
				        'taxonomies' => array( 'post_tag', 'category'),
				    );
				    register_post_type( 'destaque', $args );


				$labels = array(
	        'name' => __( 'Banner-blog' ),
	        'singular_name' => __( 'banner-blog' ),
	        'add_new' => __( 'Novo banner-blog' ),
	        'add_new_item' => __( 'Adicionar Novo banner-blog' ),
	        'edit_item' => __( 'Editar banner-blog' ),
	        'new_item' => __( 'Novo banner-blog' ),
	        'view_item' => __( 'View banner-blog' ),
	        'search_items' => __( 'Search banner-blog' ),
	        'not_found' =>  __( 'No banner-blog Found' ),
	        'not_found_in_trash' => __( 'No banner-blog found in Trash' ),
	    );
	    $args = array(
	        'labels' => $labels,
	        'has_archive' => true,
	        'public' => true,
	        'hierarchical' => false,
	        'supports' => array(
	            'title',
	            'editor',
	            'excerpt' => false,
	            'custom-fields' => false,
	            'thumbnail',
	            'page-attributes' => false,
	        ),
	        'taxonomies' => array( 'post_tag', 'category'),
	    );
	    register_post_type( 'banner-blog', $args );


	}
	add_action( 'init', 'wptutsplus_create_post_type' );


add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );

//Configurações e criação de metabox.
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Opções', 'cmb2' ),
        'object_types'  => array( 'videos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    // URL text field
    $cmb->add_field( array(
        'name' => __( 'URL do vídeo', 'cmb2' ),
        'desc' => __( 'Digite a URL do vídeo', 'cmb2' ),
        'id'   => '_url',
        'type' => 'text_url',
    ) );



    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Opções', 'cmb2' ),
        'object_types'  => array( 'banner', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    // URL text field
    $cmb->add_field( array(
        'name' => __( 'URL do vídeo', 'cmb2' ),
        'desc' => __( 'Digite a URL do vídeo', 'cmb2' ),
        'id'   => '_url',
        'type' => 'text_url',
    ) );


    // Start with an underscore to hide fields from custom fields list
    // $prefix = 'e-books';

    $cmb = new_cmb2_box( array(
      'id'            => 'E-Books',
      'title'         => __( 'Opções', 'cmb2' ),
      'object_types'  => array( 'e-book', ), // Post type
      'context'       => 'normal',
      'priority'      => 'high',
      'show_names'    => true,
      ) );

    $cmb->add_field( array(
      'name'    => 'Adicionar Arquivo',
      'desc'    => 'Adicionar arquivo em PDF, E-Books e imagens.',
      'id'      => 'wiki_test_image',
      'type'    => 'file',
      // Optional:
      'options' => array(
          'url' => false, // Hide the text input for the url
      ),
      'text'    => array(
          'add_upload_file_text' => 'Adicionar Arquivo' // Change upload button text. Default: "Add or Upload File"
      ),
      // query_args are passed to wp.media's library query.
      'query_args' => array(
          'type' => 'application/pdf', // Make library only display PDFs.
      ),

      ) );


			// Start with an underscore to hide fields from custom fields list
	    // $prefix = 'destaque';

	    $cmb = new_cmb2_box( array(
	      'id'            => 'Destaques',
	      'title'         => __( 'Opções', 'cmb2' ),
	      'object_types'  => array( 'destaque', ), // Post type
	      'context'       => 'normal',
	      'priority'      => 'high',
	      'show_names'    => true,
	      ) );

				// URL text field
				$cmb->add_field( array(
						'name' => __( 'Conteúdo do botão', 'cmb2' ),
						'desc' => __( 'Texto dentro do botão', 'cmb2' ),
						'id'   => '_url',
						'type' => 'text_url',
				) );


				// Start with an underscore to hide fields from custom fields list
	 		 // $prefix = 'destaque';

	 		 $cmb = new_cmb2_box( array(
	 			 'id'            => 'Banner-blog',
	 			 'title'         => __( 'Opções', 'cmb2' ),
	 			 'object_types'  => array( 'banner-blog', ), // Post type
	 			 'context'       => 'normal',
	 			 'priority'      => 'high',
	 			 'show_names'    => true,
	 			 ) );

				 // URL text field
		     $cmb->add_field( array(
		         'name' => __( 'Link com conteúdo do site', 'cmb2' ),
		         'desc' => __( 'Digite o link', 'cmb2' ),
		         'id'   => '_url',
		         'type' => 'text_url',
		     ) );
}

add_theme_support( 'post-thumbnails' );


// offset the main query on the home page
function tutsplus_offset_main_query ( $query ) {
     if ( $query->is_home() && $query->is_main_query() ) {
         $query->set( 'offset', '1' );
    }
 }


 //  O número 80 é a quantidade de caracteres a exibir.
function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '';
		} else {
			echo $excerpt;
		}
	} 


function granostudio_scripts_child() {

 //Desabilitar jquery
 wp_deregister_script( 'jquery' );

 // Theme stylesheet.
 wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
 // import fonts (Google Fonts)
 wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
 // Theme front-end stylesheet
 wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

 // scripts js
 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);
 // network effet home

}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
