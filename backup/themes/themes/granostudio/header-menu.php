<!-- NAV BAR ============================================================== -->
<?php
  if (is_front_page()){
    $navClassHome = 'navhome';
  } else {
    $navClassHome = '';
  }
 ?>
<nav class="navbar navbar-fixed-top navbar-default <?php echo $navClassHome; ?>" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
          <?php
          $logo_image_url = grano_get_options('design','design_logo');
          if(empty($logo_image_url) || $logo_image_url == "http://default"){
            echo bloginfo('name');

          }else{
            echo '<img src="'.$logo_image_url.'" alt="'.get_bloginfo('name').'" class="navbar-brand-img" />';
          }
           ?>
        </a>
    </div>
    <?php
    // social links
    $social_media = get_option('info_contato');
    // remover elementos que não irão aparecer no header
    if(!empty($social_media['cont_googlepmaps'])){
        unset($social_media['cont_googlepmaps']);
    }
    if(!empty($social_media['cont_telefone'])){
        unset($social_media['cont_telefone']);
    }
    if(!empty($social_media['cont_email'])){
        unset($social_media['cont_email']);
    }
    if(!empty($social_media['cont_endereço'])){
        unset($social_media['cont_endereço']);
    }

    if (!empty($social_media)) {
      ?>
      <ul class="navbar-social navbar-right">
        <?php foreach ($social_media as $key => $value) {

                switch ($key) {
                  case 'cont_facebook':
                    if($value!='Default Text'){
                      echo '<li class="icon facebook"><a href="'.$value.'">Facebook</a></li>';
                    }
                  break;
                  case 'cont_twitter':
                    if($value!='Default Text'){
                      echo '<li class="icon twitter"><a href="'.$value.'">Twitter</a></li>';
                    }
                  break;
                  case 'cont_instagram':
                    if($value!='Default Text'){
                      echo '<li class="icon instagram"><a href="'.$value.'">Instagram</a></li>';
                    }
                  break;
                  case 'cont_youtube':
                    if($value!='Default Text'){
                      echo '<li class="icon youtube"><a href="'.$value.'">Youtube</a></li>';
                    }
                  break;
                  case 'cont_flickr':
                    if($value!='Default Text'){
                      echo '<li class="icon flickr"><a href="'.$value.'">Flickr</a></li>';
                    }
                  break;
                  case 'cont_linkedin':
                    if($value!='Default Text'){
                      echo '<li class="icon linkedin"><a href="'.$value.'">Linkedin</a></li>';
                    }
                  break;
                  case 'cont_googleplus':
                    if($value!='Default Text'){
                      echo '<li class="icon googleplus"><a href="'.$value.'">Google +</a></li>';
                    }
                  break;
                }
        } ?>

        <?php
        // Link para contato
        $moduloContato = false;
        if (is_page()) {
          // verificar se página tem o modulo de contato
          $group = get_post_meta( get_the_ID(), 'page_layout', true );
          for ($i=0; $i < count($group); $i++) {
            if($group[$i]['page_modulo'] == 'Contato'){
              $moduloContato = true;
            }
          }
        }
        if($moduloContato){
          echo '<li class="icon contato"><a href="#contato">Contato</a></li>';
        }else{
          echo '<li class="icon contato"><a href="contato">Contato</a></li>';
        }
         ?>
      </ul>
      <?php
    }
     ?>

     <div id="bs-example-navbar-collapse-1" class="navbar-right navbar-collapse collapse">
       <?php
           wp_nav_menu( array(
               'menu'              => 'primary',
               'theme_location'    => 'primary',
               'depth'             => 2,
               'container'         => 'div',
               'container_class'   => 'menu',
               'container_id'      => '',
               'menu_class'        => 'nav navbar-nav',
               'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
               'walker'            => new wp_bootstrap_navwalker())
           );
       ?>
       <a href="#" class="buscar">
         <i class="fa fa-search" aria-hidden="true"></i>
       </a>
     </div>
  </div>

</nav>
<!-- /NAV BAR ============================================================== -->

<!-- Box Buscar -->
<div class="box-buscar">
  <div class="btnFechar"></div>
  <div class="form">
    <form role="search" method="get" id="searchform">
      <div class="form-group">
        <label class="sr-only" for="search">Search</label>
        <input type="text" class="form-control campo-buscar" name="s" id="s" placeholder="Escreva aqui..." />
        <div class="help hidden-xs hidden-sm">
          Pressione a tecla ENTER para buscar e ESC para fechar.
        </div>
      </div>
      <button type="submit" id="searchsubmit" class="btn btn-primary" />Pesquisar</button>
    </form>
  </div>
</div>

<!--/ Box Buscar -->
