(function($) {
  var optModelo = $(".cmb2_select");

  optModelo.change(function() {
    switch($(this).val()){
      case "Banner":
        $(this).closest('.cmb-field-list').find('.cmb-type-file-list').addClass('display');
        if($(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').hasClass('display') ||
           $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').hasClass('display')||
           $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').hasClass('display')){
             $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').removeClass('display');
             $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').removeClass('display');
           }
      break;
      case "Contato":
        //alert($(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]'));
        $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').addClass('display');
        if($(this).closest('.cmb-field-list').find('.cmb-type-file-list').hasClass('display') ||
           $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').hasClass('display') ||
           $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').hasClass('display')){
             $(this).closest('.cmb-field-list').find('.cmb-type-file-list').removeClass('display');
             $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').removeClass('display');
           }
      break;
      case "Externo":
        $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').addClass('display');
        if($(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').hasClass('display') ||
           $(this).closest('.cmb-field-list').find('.cmb-type-file-list').hasClass('display')||
           $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').hasClass('display')){
             $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').removeClass('display');
             $(this).closest('.cmb-field-list').find('.cmb-type-file-list').removeClass('display');
           }
      break;
      case "Portfolio":
            $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').removeClass('display');
            $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').removeClass('display');
            $(this).closest('.cmb-field-list').find('.cmb-type-file-list').removeClass('display');
      break;
      case "Equipe":
            $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').removeClass('display');
            $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').removeClass('display');
            $(this).closest('.cmb-field-list').find('.cmb-type-file-list').removeClass('display');
      break;
      case "Clientes":
             $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').removeClass('display');
             $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').removeClass('display');
             $(this).closest('.cmb-field-list').find('.cmb-type-file-list').removeClass('display');
      break;
      case "Bannerconteudo":
          $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').addClass('display');
          if($(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').hasClass('display') ||
             $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').hasClass('display') ||
             $(this).closest('.cmb-field-list').find('.cmb-type-file-list').hasClass('display')){
              $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').hasClass('display')
               $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').removeClass('display');
               $(this).closest('.cmb-field-list').find('.cmb-type-file-list').removeClass('display');
             }
      break;
    }


  });

  // funcao init
  optModelo.each(function(){
    switch(  $(this).find('option:selected').val()){
      case "Banner":
        $(this).closest('.cmb-field-list').find('.cmb-type-file-list').addClass('display');
      break;
      case "Contato":
        $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').addClass('display');
      break;
      case "Externo":
        $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').addClass('display');
      break;
      case "Bannerconteudo":
      $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').addClass('display');
      break;
    }
  });

 })(jQuery);
