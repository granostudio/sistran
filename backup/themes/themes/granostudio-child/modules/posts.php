<?php

/**
* Módulo:
* ***** Posts - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_posts(){
    ?>
    <!-- <div class="container grano-posts">
      <div class="row"> -->
        <!-- Banner -->

        <!-- / Banner -->
        <?php
           $args = array( 'post_type' => 'post', 'posts_per_page' => 10 );
        	 $loop = new WP_Query( $args );
        	while ( $loop->have_posts() ) :
            $loop->the_post();
            $post_id = get_the_ID();
        	  ?>

            <div class="col-sm-6 col-md-4 post">
              <div class="social-share <?php if (has_post_thumbnail()){ echo 'thumb';}?>">
                Compartilhe
                <?php GranoSocialShare(get_the_permalink()) ?>
              </div>

              <a href="<?php echo get_the_permalink(); ?>" class="post-link"></a>
              <div class="post-border">
                <?php if (has_post_thumbnail()){?>
                  <?php $thumbUrl = get_the_post_thumbnail_url($post_id, 'medium' ); ?>
                  <div class="img-thumb" style="background-image: url(<?php echo $thumbUrl; ?>)"></div>
                <div class="post-inner thumb">
                <?php } else {
                ?>
                <div class="post-inner">
                <?php

                };
                  $categories = get_the_category( $post_id );
                  $i=1;
                  $cats = array();
                  foreach( $categories as $category ) {
                    $cat = get_category( $category );
                    $cats = array( 'name' => $cat->name, 'slug' => $cat->slug, 'link'=> $cat->term_id );
                    $catlink = get_category_link($cats['link']);
                    if($i==count($categories)){
                      if ($cats['slug']!="sem-categoria") {
                        echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>";
                      }else{
                        echo "<a href='".$catlink."' class='categoria'></a>";
                      }
                    } else {
                      if ($cats['slug']!="sem-categoria") {
                        echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>".', ';
                      }else{
                        echo "<a href='".$catlink."' class='categoria'></a>";
                      }
                    }
                    $i++;
                  }
                  ?>

                  <h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h4>
                    <?php the_excerpt(); ?>
                  <!-- <a class="btn-default btn" href="<?php //echo get_the_permalink(); ?>">Leia mais</a> -->
                </div>
              </div>
            </div>
            <?php
        	endwhile; ?>
      </div>
    </div>

    <?php
}
 ?>
