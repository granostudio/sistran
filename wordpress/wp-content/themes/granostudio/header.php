<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */


?>

	<!-- HEADER -->
	<!DOCTYPE html>

	<html lang="UTF-8" >
	<head>
	<base href="/">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
		<?php wp_head(); ?>			


		<!-- METATAGS -->
		<meta property="og:url" content="<?php echo get_permalink(); ?>" />
		<?php
		// site title
		$blog_title = get_bloginfo( 'name' );
		if(is_home(	)){
		?>
		<meta property="og:title" content="<?php echo $blog_title; ?>" />
		<?php
		} else{
		?>
		<meta property="og:title" content="<?php echo get_the_title(); ?>" />
		<?php
		}
		?>

		<meta property="og:type" content="website" />

		<?php if(is_home(	)){
		// site description
		$blog_desc = get_bloginfo( 'description' );
		?>
		<meta property="og:description" content="<?php echo $blog_desc; ?>" />
		<?php } else{ ?>
		<meta property="og:description" content="<?php echo get_the_content(); ?>" />
			<?php } ?>

		<meta property="og:site_name" content="<?php echo $blog_title; ?>" />

		<?php
		//imagem default ou thumbnail post
		$urlimg = '';
		$id = get_the_ID();
		if ( has_post_thumbnail($id) ) {
				$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
			 	$urlimg = $large_image_url[0];
		 } else {
			 $urlimg = grano_get_options('seo','seo_img');
		}
			?>
		<meta property="og:image" content="<?php echo $urlimg; ?>" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="200" />
		<meta property="og:image:height" content="200" />

		<!-- <base href=" /" dirName=" " lang="<?php //echo $lang ?>" /> -->

		<?php
		$favicon = grano_get_options('design','design_favicon');
		 ?>
		<link rel="shortcut icon" href="<?php echo $favicon; ?>">

		<!-- /METATAGS -->

		
		<!-- Hotjar Tracking Code for http://www.sistran.com.br/ -->
		<script>
		    (function(h,o,t,j,a,r){
		        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
		        h._hjSettings={hjid:869189,hjsv:6};
		        a=o.getElementsByTagName('head')[0];
		        r=o.createElement('script');r.async=1;
		        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
		        a.appendChild(r);
		    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
		</script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118670472-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-118670472-1');
		</script>


		<!-- Script de desativação das funções do jquery mobile -->
		<script>
			$(document).ready(function(){
			  $('[data-toggle="tooltip"]').tooltip();
			}); 

			$(document).ready(function(){
			    $("a").each(function(){
			          $(this).attr("rel","external");
			    });
			}); 	

			$(document).ready(function(){
			    $(".wpcf7-form").each(function(){
			          $(this).attr("data-ajax","false");
			    });
			}); 			
		</script>

</head><!-- /Header -->
<body ng-app="wp" >


<div >
<!-- Header Menu -->
<?php include 'header-menu.php' ?>
<!-- /Header Menu -->

<!-- Scroll Animation Grano -->
<div class="scroll-pointer"></div>
<div class="scroll-ani"></div>
