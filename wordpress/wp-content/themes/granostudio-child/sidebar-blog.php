<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<div class="col-sm-4">
  <h3 style="margin-top: 0px;margin-bottom: 16px;">ÚLTIMAS POSTAGENS</h3>  
</div>

<?php
       $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
       $args = array( 'post_type' => 'post', 'posts_per_page' => 2, 'paged' => $paged, 'page' => $paged);
       $loop = new WP_Query( $args );

       if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

    
    
    <div class="col-sm-4">

        <div class="posts">
         <!-- <a href="<?php echo get_the_permalink(); ?>"> -->
            <?php the_post_thumbnail( ); ?>
          <!-- </a> -->
            <div class="excerpt">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
              <p><?php the_excerpt(); ?></p>
            </div>

          <div class="mask-post">
            <a href="<?php echo get_the_permalink(); ?>">
            <div class="text">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><?php echo get_the_title(); ?></h3>
              <button type="button" class="botao" name="button">Leia Mais</button>
            </div>
            </a>
          </div>
       </div>

    </div>

        <?php endwhile; ?>
        <?php endif; ?>