<?php get_header(); ?>

<div class="fale-conosco">
  <div class="container">

    <h1>Venha fazer parte da Sistran Brasil!</h1>
    <p class="col-sm-8 col-sm-offset-2">Junte-se a um time de profissionais altamente capacitados em Seguros. Preencha o formulário:</p>

    <div class="col-sm-8 col-sm-offset-2">

      <?php echo do_shortcode('[contact-form-7 id="111" title="Trabalhe Conosco"]'); ?>
      

    <!-- Begin MailChimp Signup Form -->
<!--     <div id="mc_embed_signup">
      <form action="https://sise.us12.list-manage.com/subscribe/post?u=37f7054b1c1bd1f0613081348&amp;id=191577712b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        
        <div id="mc_embed_signup_scroll">
        
          <div class="mc-field-group">
            <input type="text" value="" name="NAME" class="required" id="mce-NAME" placeholder="Nome">
          </div>
          <div class="mc-field-group">
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
          </div>
          <div class="mc-field-group">
            <input type="text" value="" name="TEL" class="" id="mce-TEL" placeholder="Telefone">
          </div>
          <div class="mc-field-group">
            <input type="text" value="" name="ULTCARGO" class="" id="mce-ULTCARGO" placeholder="Último Cargo">
          </div>
          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>    

          <div style="position: absolute; left: -5000px;" aria-hidden="true">
            <input type="text" name="b_37f7054b1c1bd1f0613081348_191577712b" tabindex="-1" value="">
          </div>
          <div class="clear">
            <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button botao botao-home">
          </div>

        </div>
      </form>
    </div> -->

    <!--End mc_embed_signup-->

    </div>

    <div class="col-sm-12">
      <a href="/sobre/"><button type="button" class="botao botao-home" name="button">Sobre a Sistran</button></a>
    </div>

  </div>

</div>

<?php get_footer(); ?>
