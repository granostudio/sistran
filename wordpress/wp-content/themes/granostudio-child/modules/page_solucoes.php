<?php get_header(); ?>

<!-- Div1 -->
<div class="solucoes">
	<h1>Soluções de Negócios</h1> 
    <hr class="titulo" style="width: 18%;">	
    <p class="paragrafos-ini" style="margin-bottom: 0;"><?php the_field('paragrafo_1_solucoes'); ?></p>

    <div class="container">
      <div class="div1-solucoes row"> 
         <p class="paragrafos-ini"><?php the_field('paragrafo_2_solucoes'); ?></p>
	    <div class="col-sm-3">
  			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-consultoria.png">
  			<p class="titulo-solucoes" data-toggle="modal" data-target=".modal-consultoria"><?php the_field('icone_1_solucoes'); ?></p>
  			<!-- <p>loren ipsum dolor sit ament adispicing consectetur</p> -->		      	
	    </div>     

	    <div class="col-sm-3">
  			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-projetos.png">
  			<p class="titulo-solucoes" data-toggle="modal" data-target=".modal-projetos"><?php the_field('icone_2_solucoes'); ?></p>
  			<!-- <p>loren ipsum dolor sit ament adispicing consectetur</p> -->	      	
	    </div>

	    <div class="col-sm-3">
  			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-body.png">
  			<p class="titulo-solucoes" data-toggle="modal" data-target=".modal-alocacao"><?php the_field('icone_3_solucoes'); ?></p>
  			<!-- <p>loren ipsum dolor sit ament adispicing consectetur</p> -->		      	
	    </div>

	    <div class="col-sm-3">
  			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-outsourcing.png">
  			<p class="titulo-solucoes" data-toggle="modal" data-target=".modal-outsourcing"><?php the_field('icone_4_solucoes'); ?></p>
  			<!-- <p>loren ipsum dolor sit ament adispicing consectetur</p> -->		      	
	    </div>
      </div>
    </div>


    <!-- Modal-Consultoria -->
      <div class="modal fade modal-consultoria" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="">
              <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
            </div>
            <div class="banner-modal1"></div>
            <div class="conteudo-modal">
              <h1><?php the_field('icone_1_solucoes'); ?></h1>
              <hr class="titulo">
              <p class="pos-titulo"><?php the_field('postitulo_modal1_solucoes'); ?></p>
              <?php the_field('campo_modal1_solucoes'); ?>
              <hr>                               
            </div>
            <div class="row">                  
              <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
            </div>
          </div>
        </div>
      </div>

    <!-- Modal-Projetos -->
      <div class="modal fade modal-projetos" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="">
              <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
            </div>
            <div class="banner-modal1"></div>
            <div class="conteudo-modal">
              <h1><?php the_field('icone_2_solucoes'); ?></h1>
              <hr class="titulo">
              <p class="pos-titulo"><?php the_field('postitulo_modal2_solucoes'); ?></p>
              <?php the_field('campo_modal2_solucoes'); ?>
              <hr>                               
            </div>
            <div class="row">                  
              <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
            </div>
          </div>
        </div>
      </div>  

    <!-- Modal-Alocação -->
      <div class="modal fade modal-alocacao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="">
              <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
            </div>
            <div class="banner-modal1"></div>
            <div class="conteudo-modal">
              <h1><?php the_field('icone_3_solucoes'); ?></h1>
              <hr class="titulo">
              <p class="pos-titulo"><?php the_field('postitulo_modal3_solucoes'); ?></p>
              <?php the_field('campo_modal3_solucoes'); ?>
              <hr>                               
            </div>
            <div class="row">                  
              <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
            </div>
          </div>
        </div>
      </div> 

    <!-- Modal-Outsourcing -->
      <div class="modal fade modal-outsourcing" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="">
              <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
            </div>
            <div class="banner-modal1"></div>
            <div class="conteudo-modal">
              <h1><?php the_field('icone_4_solucoes'); ?></h1>
              <hr class="titulo">
              <p class="pos-titulo"><?php the_field('postitulo_modal4_solucoes'); ?></p>
              <?php the_field('campo_modal4_solucoes'); ?>
              <hr>                               
            </div>
            <div class="row">                  
              <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
            </div>
          </div>
        </div>
      </div>             


    <div class="cerebro">  
        <div class="bullet-cerebro acender-1" data-toggle="modal" data-target=".bd-example-modal-lg18"><p>+</p></div><!-- nth-child(1) -->

        <div class="bullet-cerebro acender-1" data-toggle="modal" data-target=".bd-example-modal-lg19"><p>+</p></div><!-- nth-child(2) -->

        <div class="bullet-cerebro acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg20"><p>+</p></div><!-- nth-child(3) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg21"><p>+</p></div><!-- nth-child(4) -->

        <div class="bullet-cerebro acender-1" data-toggle="modal" data-target=".bd-example-modal-lg22"><p>+</p></div><!-- nth-child(5) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg23"><p>+</p></div><!-- nth-child(6) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg24"><p>+</p></div><!-- nth-child(7) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg25"><p>+</p></div><!-- nth-child(8) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg26"><p>+</p></div><!-- nth-child(9) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg27"><p>+</p></div><!-- nth-child(10) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg28"><p>+</p></div><!-- nth-child(11) --> 

        <div class="bullet-cerebro acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg29"><p>+</p></div><!-- nth-child(12) -->

        <div class="bullet-cerebro acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg30"><p>+</p></div><!-- nth-child(13) --> 

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg31"><p>+</p></div><!-- nth-child(14) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg32"><p>+</p></div><!-- nth-child(15) --> 

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg33"><p>+</p></div><!-- nth-child(16) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left apagar-botao1-right" data-toggle="modal" data-target=".bd-example-modal-lg34"><p>+</p></div><!-- nth-child(17) -->

        
        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 

            <div class="texto-bullet acender-1"><?php echo get_the_title(); ?></div><!-- nth-child(18/1) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 1,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 

            <div class="texto-bullet acender-1"><?php echo get_the_title(); ?></div><!-- nth-child(19/2) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 2,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 

            <div class="texto-bullet acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(20/3) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 3,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 

            <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(21/4) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>      


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 4,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 

            <div class="texto-bullet acender-1"><?php echo get_the_title(); ?></div><!-- nth-child(22/5) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>     


        <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right">Big Data</div><!-- nth-child(23/6) -->
        

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 5,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>         

            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left"><?php echo get_the_title(); ?></div><!-- nth-child(24/7) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 6,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>   

            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left"><?php echo get_the_title(); ?></div><!-- nth-child(25/8) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>       
        

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 7,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>  

            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left"><?php echo get_the_title(); ?></div><!-- nth-child(26/9) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>      


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 8,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>  
        
            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left"><?php echo get_the_title(); ?></div><!-- nth-child(27/10) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>  


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 9,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>              
        
            <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(28/11) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>    


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 10,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 
        
            <div class="texto-bullet acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(29/12) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 11,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 
        
            <div class="texto-bullet acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(30/13) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 12,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>       
        
            <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(31/14) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 13,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 

            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(32/15) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 14,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>  
        
            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right"><?php echo get_the_title(); ?></div><!-- nth-child(33/16) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>   


        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 15,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?> 
        
            <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left apagar-botao1-right"><?php echo get_the_title(); ?></div><!-- nth-child(34/17) -->

        <?php endwhile; ?>
        <?php } wp_reset_postdata(); ?>     
        
    </div>


      <!-- Modal 1/targe lg18/bullet-1 -->

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'bullet-cerebro',
          'posts_per_page' => 1,
          'orderby' => 'date', 
          'order' => 'ASC'
      )); 

      if ( $query->have_posts() ) {
      while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg18" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>                               
                </div>
                <div class="row">                  
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

        <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>


        <!-- Modal 2/targe lg19/bullet-2 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 1,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg19" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>                
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div> 

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 3/targe lg20/bullet-3 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 2,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg20" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 4/targe lg21/bullet-4 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 3,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg21" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>       
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

        <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>   


        <!-- Modal 5/targe lg22/bullet-5 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 4,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg22" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

        <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>

        <!-- Modal 6/targe lg23/bullet-6 -->
<!--           <div class="modal fade bd-example-modal-lg23" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Big Data</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b></b></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div> -->


        <!-- Modal 7/targe lg24/bullet-7 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 5,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg24" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div> 

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 8/targe lg25/bullet-8 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 6,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg25" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 9/targe lg26/bullet-9 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 7,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg26" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 10/targe lg27/bullet-10 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 8,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg27" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 11/targe lg28/bullet-11 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 9,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg28" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


        <!-- Modal 12/targe lg29/bullet-12 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 10,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg29" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                    <h1>Soluções de Negócios</h1>
                    <hr class="titulo">
                    <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                    <?php the_field('texto_modal'); ?> 
                    <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>   

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>         


        <!-- Modal 13/targe lg30/bullet-13 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 11,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg30" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>  


        <!-- Modal 14/targe lg31/bullet-14 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 12,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg31" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>  


        <!-- Modal 15/targe lg32/bullet-15 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 13,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg32" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>  

        <!-- Modal 16/targe lg33/bullet-16 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 14,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg33" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>  


        <!-- Modal 17/targe lg34/bullet-17 -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'bullet-cerebro',
            'posts_per_page' => 1,
            'offset' => 15,
            'orderby' => 'date', 
            'order' => 'ASC'
        )); 

        if ( $query->have_posts() ) {
        while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="modal fade bd-example-modal-lg34" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>                
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <h1>Soluções de Negócios</h1>
                  <hr class="titulo">
                  <p class="pos-titulo"><?php echo get_the_title(); ?></p>
                  <?php the_field('texto_modal'); ?> 
                  <hr>
                </div>
                <div class="row">
                  <a href="/contato/"><button type="button" class="botao" name="button">Entre em contato</button></a>  
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>  
    

  <div class="container descricao">
    <?php the_field('campo_pos_cerebro_solucoes'); ?>

  </div>

  <div class="graficos-solucoes container">  

    <div class="col-sm-12 col-md-8">           
      <img src="<?php echo get_stylesheet_directory_uri();?>/img/logo-solucoes-1.png" class="">
      <img src="<?php echo get_stylesheet_directory_uri();?>/img/logo-solucoes-2.png" class="">
    </div>      

    <div class="col-sm-12 col-md-4 coluna-grafico-3">
      <div class="col-sm-5">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/velocimetros-sem-fundo.png" class="">
      </div>
      <div class="col-sm-7">
        <p>Innovations and speed from external demands to a System of Engagement</p>
        <p>Speed from Internal demands to a Systems of Records</p>
      </div>
    </div> 

  </div>

</div>

<div class="solucoes-div2">
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1">
			<h3><?php the_field('titulo_campo_amarelo_solucoes1'); ?></h3>
			<?php the_field('texto_campo_amarelo_solucoes1'); ?>

			<h3><?php the_field('titulo_campo_amarelo_solucoes2'); ?></h3>
			<?php the_field('texto_campo_amarelo_solucoes2'); ?>
		</div>
	</div>
</div>    

<div class="solucoes-div3">
	<div class="container">
    <div class="row">
      <h1>Parceiros</h1>
      <hr class="titulo">  
    </div>			

    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">

        <?php
         $args = array( 'post_type' => 'parceiros');
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        <div class="col-sm-3">
          <?php the_post_thumbnail( ); ?>
        </div>

        <?php endwhile; // end of the loop. ?>
        <?php endif; ?>

      </div>
    </div>    	

    <div class="row">
        <a href="/parcerias/"><button type="button" class="botao" name="button">Conheça todos nossos parceiros</button></a>  
    </div>
      
	</div>
</div>

<div class="solucoes-div4">
	<h1>Produtos</h1>
    <hr class="titulo">	

    <div class="container" style="width: 100%;margin: 0 0 50px;">
      <div class="row">  

      <?php
       $args = array( 'post_type' => 'produtos');
       $loop = new WP_Query( $args );

       if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>
   
      <div class="col-sm-4" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
      	<p><?php echo get_the_title(); ?></p>
      	<p><?php the_content(); ?></p>
      </div>

      <?php endwhile; // end of the loop. ?>
      <?php endif; ?>

      </div>
    </div>

</div>

<button onclick="location.href='/contato/'" type="button" class="botao botao-final" name="button">Entre em contato</button>  



<?php get_footer(); ?>