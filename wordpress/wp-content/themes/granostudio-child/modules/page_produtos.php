<?php get_header(); ?>

<!-- Div1 -->
<div class="solucoes">
	<h1>Soluções de Negócios</h1>
    <hr class="titulo" style="width: 18%;">	
    <p class="paragrafos-ini">Veja como a Sistran pode ajudar sua Seguradora nos mais variados desafios de negócios.</p>

    <div class="container">
      <div class="div1-solucoes row">
         <p class="paragrafos-ini">Podemos oferecer nossos produtos e serviços através dos seguintes modelos</p>  
	      <div class="col-sm-3">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-consultoria.png">
			<p class="titulo-solucoes">Consultoria</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>

	      <div class="col-sm-3">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-projetos.png">
			<p class="titulo-solucoes">Projetos</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>

	      <div class="col-sm-3">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-body.png">
			<p class="titulo-solucoes">Body Shop</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>

	      <div class="col-sm-3">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-outsourcing.png">
			<p class="titulo-solucoes">Outsourcing</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>
      </div>
    </div>


    <div class="cerebro"> 
        <div class="bullet-cerebro acender-1" data-toggle="modal" data-target=".bd-example-modal-lg18"><p>+</p></div><!-- nth-child(1) -->

        <div class="bullet-cerebro acender-1" data-toggle="modal" data-target=".bd-example-modal-lg19"><p>+</p></div><!-- nth-child(2) -->

        <div class="bullet-cerebro acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg20"><p>+</p></div><!-- nth-child(3) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg21"><p>+</p></div><!-- nth-child(4) -->

        <div class="bullet-cerebro acender-1" data-toggle="modal" data-target=".bd-example-modal-lg22"><p>+</p></div><!-- nth-child(5) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg23"><p>+</p></div><!-- nth-child(6) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg24"><p>+</p></div><!-- nth-child(7) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg25"><p>+</p></div><!-- nth-child(8) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg26"><p>+</p></div><!-- nth-child(9) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg27"><p>+</p></div><!-- nth-child(10) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg28"><p>+</p></div><!-- nth-child(11) --> 

        <div class="bullet-cerebro acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg29"><p>+</p></div><!-- nth-child(12) -->

        <div class="bullet-cerebro acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg30"><p>+</p></div><!-- nth-child(13) --> 

        <div class="bullet-cerebro acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg31"><p>+</p></div><!-- nth-child(14) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg32"><p>+</p></div><!-- nth-child(15) --> 

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg33"><p>+</p></div><!-- nth-child(16) -->

        <div class="bullet-cerebro acender-1 apagar-botao1-left apagar-botao3-left apagar-botao1-right" data-toggle="modal" data-target=".bd-example-modal-lg34"><p>+</p></div><!-- nth-child(17) -->


        <div class="texto-bullet acender-1">Workflow/BPM/ECM</div><!-- nth-child(18/1) -->

        <div class="texto-bullet acender-1">Specialized Components</div><!-- nth-child(19/2) -->

        <div class="texto-bullet acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right">Cotador</div><!-- nth-child(20/3) -->

        <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right">Portal Corretor</div><!-- nth-child(21/4) -->

        <div class="texto-bullet acender-1">Business Intelligence</div><!-- nth-child(22/5) -->

        <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right">Big Data</div><!-- nth-child(23/6) -->

        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left">Resseguros</div><!-- nth-child(24/7) -->

        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left">Cosseguros</div><!-- nth-child(25/8) -->
        
        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left">Contábil e Financeiro</div><!-- nth-child(26/9) -->
        
        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left">Comissões</div><!-- nth-child(27/10) -->
        
        <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right">CRM<br>Contact Center</div><!-- nth-child(28/11) -->
        
        <div class="texto-bullet acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right">Gestão da Distribuição</div><!-- nth-child(29/12) -->
        
        <div class="texto-bullet acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right">Subscrição de riscos</div><!-- nth-child(30/13) -->
        
        <div class="texto-bullet acender-1 apagar-botao1-right apagar-botao3-right">Portal Segurado</div><!-- nth-child(31/14) -->

        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right">Gestão de Apólice</div><!-- nth-child(32/15) -->
        
        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right">Faturamento e cobrança</div><!-- nth-child(33/16) -->
        
        <div class="texto-bullet acender-1 apagar-botao1-left apagar-botao3-left apagar-botao1-right">Gestão de Sinistro</div><!-- nth-child(34/17) -->



        <div class="container">
            <div class="row">
                <div class="col-sm-2 front-back-office">
                    <div><p>Front Office</p></div>
                    <div class="botao-office botao1-left"><p>Cotação e Proposta</p></div>
                    <div class="botao-office botao2-left"><p>Endosso</p></div>
                    <div class="botao-office botao3-left"><p>Sinistro</p></div>
                </div>

                <div class="col-sm-2 col-sm-offset-8 front-back-office">
                    <div><p>Back Office</p></div>
                    <div class="botao-office botao1-right"><p>Subscrição e Emissão</p></div>
                    <div class="botao-office botao2-right"><p>Apólice/Endosso</p></div>
                    <div class="botao-office botao3-right"><p>Sinistro</p></div>
                </div>
            </div>
        </div>    
        
    </div>

    <div class="lista-cerebro">

      <div class="container">
            <div class="row">
                <div class="col-sm-2 front-back-office">
                    <div><p>Front Office</p></div>
                    <div class="botao-office botao1-left"><p>Cotação e Proposta</p></div>
                    <div class="botao-office botao2-left"><p>Endosso</p></div>
                    <div class="botao-office botao3-left"><p>Sinistro</p></div>
                </div>

                <div class="col-sm-2 col-sm-offset-8 front-back-office">
                    <div><p>Back Office</p></div>
                    <div class="botao-office botao1-right"><p>Subscrição e Emissão</p></div>
                    <div class="botao-office botao2-right"><p>Apólice/Endosso</p></div>
                    <div class="botao-office botao3-right"><p>Sinistro</p></div>
                </div>
            </div>
        </div>

        <div class="container">
          <ul>
            <li class="acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg23">Big Data</li>

            <li class="acender-1" data-toggle="modal" data-target=".bd-example-modal-lg22">Business Intelligence</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg27">Comissões</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg26">Contábil e Financeiro</li>

            <li class="acender-1 acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg28">CRM Contact Center</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg25">Cosseguros</li>

            <li class="acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg20">Cotador</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg33">Faturamento e Cobrança</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg32">Gestão de Apólice</li>

            <li class="acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg29">Gestão da Distribuição</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left apagar-botao1-right" data-toggle="modal" data-target=".bd-example-modal-lg34">Gestão de Sinistro</li>

            <li class="acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg21">Portal Corretor</li>

            <li class="acender-1 apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg31">Portal Segurado</li>

            <li class="acender-1 apagar-botao1-left apagar-botao3-left" data-toggle="modal" data-target=".bd-example-modal-lg24">Resseguros</li>

            <li class="acender-1"  data-toggle="modal" data-target=".bd-example-modal-lg19">Specialized Components</li>

            <li class="acender-1 apagar-botao3-left apagar-botao1-right apagar-botao3-right" data-toggle="modal" data-target=".bd-example-modal-lg30">Subscrição de riscos</li>

            <li class="acender-1" data-toggle="modal" data-target=".bd-example-modal-lg18">Workflow/BPM/ECM</li>
          </ul>
        </div>

    </div><!-- fim lista-cerebro -->


    <!-- Modal 1/targe lg18/bullet-1 -->
          <div class="modal fade bd-example-modal-lg18" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Workflow/BPM/ECM</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>O time to market da Seguradora está adequado para que sejam apresentadas novas ofertas  de acordo com a necessidade do mercado?</b></p>
                  <p>A Sistran utiliza ferramentas para captura de requisitos e desenho de soluções complementada por gerador de código Java, o que reduz o time to market.</p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 2/targe lg19/bullet-2 -->
          <div class="modal fade bd-example-modal-lg19" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Specialized Components</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b></b></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div> 

        <!-- Modal 3/targe lg20/bullet-3 -->
          <div class="modal fade bd-example-modal-lg20" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Cotador</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>A Seguradora consegue se adaptar às flutuações do mercado em todos os canais e de forma automática?</b></p>
                  <p>Por meio de processo cognitivo e machine learning, é possível que sejam identificadas soluções que se adaptam às condições de momento do mercado. </p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 4/targe lg21/bullet-4 -->
          <div class="modal fade bd-example-modal-lg21" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Portal Corretor</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>O portal para os corretores da sua Seguradora tem algum diferencial?</b></p>
                  <p>Uma forma de diferenciar o atendimento aos parceiros é alavancar o portal corretor com ferramentas de Business Intelligence (BI) e Next Best Action (NBA). Isso proporciona uma navegação assertiva, com funcionalidades mais atrativas para o parceiro.</p>
                </div>
              </div>
            </div>
          </div> 

        <!-- Modal 5/targe lg22/bullet-5 -->
          <div class="modal fade bd-example-modal-lg22" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Business Itelligence</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>A Seguradora tem aproveitado todo o potencial do cross-selling?</b></p>
                  <p>A Sistran dispõe de ferramentas analíticas e preditivas para elevar o seu indicador médio de apólices por domicílio.</p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 6/targe lg23/bullet-6 -->
          <div class="modal fade bd-example-modal-lg23" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Big Data</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b></b></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 7/targe lg24/bullet-7 -->
          <div class="modal fade bd-example-modal-lg24" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Resseguros</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>A sua Seguradora está satisfeita com a gestão de contratos de resseguros?</b></p>
                  <p>Ferramentas para consolidação de informações sobre operações de resseguros são essenciais para melhorar a eficiência da área e diminuir perdas.</p>
                </div>
              </div>
            </div>
          </div> 

        <!-- Modal 8/targe lg25/bullet-8 -->
          <div class="modal fade bd-example-modal-lg25" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Cosseguros</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>Os resultados das operações de cosseguros são fáceis de acessar e analisar?</b></p>
                  <p>É preciso unir ferramentas de consolidação para apurar o seu combined de cosseguro com acuracidade e assertividade.</p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 9/targe lg26/bullet-9 -->
          <div class="modal fade bd-example-modal-lg26" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Contábil e Financeiro</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b></b></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 10/targe lg27/bullet-10 -->
          <div class="modal fade bd-example-modal-lg27" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Comissões</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b></b></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 11/targe lg28/bullet-11 -->
          <div class="modal fade bd-example-modal-lg28" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>CRM Contact Center</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>O Tempo Médio de Atendimento (TMA) da Seguradora é melhor do que o dos concorrentes?</b></p>
                  <p>A tecnologia de machine learning e a consolidação de informações em uma só tela pode ajudar a melhorar substancialmente o tempo de resposta de sua equipe aos segurados.</p>
                </div>
              </div>
            </div>
          </div>



        <!-- Modal 12/targe lg29/bullet-12 -->
          <div class="modal fade bd-example-modal-lg29" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Gestão da Distribuição</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                    <p>As operações de affinity da Seguradora estão rentáveis, adequadas e satisfazendo os acionistas?</p>
                    <p>Para otimizar os resultados, a Sistran sugere que se utilizem ferramentas para apoiar o desenho de ofertas, incluindo a flexibilidade por geografia, perfil e produtos. Isso vai permitir uma abordagem mais customizada e adequada à realidade de cada cliente.</p>
                </div>
              </div>
            </div>
          </div>            

        <!-- Modal 13/targe lg30/bullet-13 -->
          <div class="modal fade bd-example-modal-lg30" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Subscrição de riscos</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>O processo de underwriting da Seguradora é autoajustável aos riscos?</b></p>
                  <p>Para isso seja possível, é necessário possuir algoritmos Real Time Decisioning (RTD) e Next Best Action (NBA) que calibrem, de forma automática, a aceitação do risco. Ferramentas de detecção de fraude também garantem mais assertividade.</p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 14/targe lg31/bullet-14 -->
          <div class="modal fade bd-example-modal-lg31" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Portal Segurado</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>O segurado encontra exatamente o que procura no portal da Seguradora? Você consegue identificar seu grau de satisfação?</b></p>
                  <p>A Sistran consegue proporcionar informação consolidada da posição do segurado, ofertas de produtos e serviços omnichannel. Isso permite um melhor conhecimento sobre a evolução do atendimento na Seguradora e medição mais assertiva da satisfação do usuário, independentemente da plataforma que ele utilizou para fazer o contato.</p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 15/targe lg32/bullet-15 -->
          <div class="modal fade bd-example-modal-lg32" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Gestão de Apólice</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>A Seguradora tem problemas relacionados a lançamentos e configurações de produtos?</b></p>
                  <p>A Sistran tem plataforma de Seguros altamente parametrizável,  que pode ser customizada de acordo com as especificidades de cada negócio. </p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 16/targe lg33/bullet-16 -->
          <div class="modal fade bd-example-modal-lg33" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Faturamento e cobrança</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>Os processos de faturamento da Seguradora são desempenhados de forma adequada?</b></p>
                  <p>É necessário utilizar ferramentas para verificação e integração de informações, de forma que o faturamento da Seguradora não sofra falhas ou atrasos que comprometam as operações.</p>
                </div>
              </div>
            </div>
          </div>

        <!-- Modal 17/targe lg34/bullet-17 -->
          <div class="modal fade bd-example-modal-lg34" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="">
                  <button type="button" class="btn btn-default btn-modal" data-dismiss="modal"><img src="<?php echo get_stylesheet_directory_uri();?>/img/favicon-exit.png"></button>
                </div>
                <h1>Gestão de Sinistros</h1>
                <hr class="titulo">
                <div class="banner-modal1"></div>
                <div class="conteudo-modal">
                  <p><b>A porcentagem de detecção de fraudes em sinistros da sua Seguradora é suficiente ou ocorrências importantes foram deixadas para trás?</b></p>
                  <p>Por meio de ferramentas analíticas e preditivas especializadas em detecção de fraudes, é possível aumentar o índice de identificação de casos suspeitos,  o que tende a minimizar significativamente as perdas das Seguradoras.</p>
                </div>
              </div>
            </div>
          </div>
    

</div>

<div class="solucoes-div2">
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1">
			<h3>Por que muitos projetos de tecnologia em Seguradoras falham?</h3>
			<p>São vários os motivos: os mais comuns são falta de especialização dos fornecedores de TI no mercado de Seguros, que possui uma dinâmica própria e com altos níveis de regulação. Por esse mesmo motivo, produtos trazidos de fora e não adaptados à realidade local também se mostram incapazes de resolver problemas e necessidades nacionais.</p>

			<h3>Você procura uma solução de negócios para Seguradora com bom equilíbrio custo-benefício?</h3>
			<p>A resposta a essa questão são ofertas que possuam padrão internacional, metodologia comprovada e preço local. <a href="">Saiba mais em "Sobre a Sistran."</a></p>
		</div>
	</div>
</div>

<div class="solucoes-div3">
	<div class="container">
		<h1>Parceiros</h1>
    	<hr class="titulo">	

    	<div class="col-sm-10 col-sm-offset-1">
    		<div class="col-sm-4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice1@2x.png"></div>
    		<div class="col-sm-4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png"></div>
    		<div class="col-sm-4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png"></div>
    	</div>
	</div>
</div>

<div class="solucoes-div4">
	<h1>Soluções</h1>
    <hr class="titulo">	

    <div class="col-sm-4">
    	<p>SISE RC</p>
    	<p>Gestão Produtos - Responsabilizade Civil</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE RD</p>
    	<p>Gestão Produtos - Risco Diversos</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE RE (P&C ERP)</p>
    	<p>Gestão Produtos para Ramos Elementares</p>
    </div>

    <div class="col-sm-4">
    	<p>RESSEGUROS</p>
    	<p>Gestão dos Contatos de Resseguros</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE CONNECT</p>
    	<p>Plataforma de vendas que compreende todo o ciclo do negócio, da cotação até a proposta</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE VIDA</p>
    	<p>Solução global integrada que permite gerenciamento completo de produtos de Seguro de Vida</p>
    </div>

</div>

<?php get_footer(); ?>