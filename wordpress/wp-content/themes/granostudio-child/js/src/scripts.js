$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,
      nav: true

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".slider-active").owlCarousel({
  loop:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
  });
});


$('#myCarousel-principal').carousel({
  interval: 5000,
  pause: null,
})

$('[id^=myCarousel-principal-]').click( function(){
    var id = this.id.substr(this.id.lastIndexOf("-") + 1);
    var id = parseInt(id);  
    $('#myCarousel-principal').carousel(id);
}); 

$('#article-photo-carousel').carousel({
  interval: 5000,
  pause: null,
})

$('#myCarousel-blog').carousel({
  interval: 5000,
  pause: null,
})

$('#myCarousel-blog2').carousel({
  interval: 5000,
  pause: null,
})

// bxslider crousel ===============================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });


// Back to Top ===============================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ==============================================


// Grano Scroll ANIMATION ====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ==================================

// / Banner home div4 ========================================
jQuery(document).ready(function($) {

    $('#myCarousel').carousel({ 
            interval: 4000 
    });

    $('#carousel-text').html($('#slide-content-0').html());

    // $('#slide-content').carousel({ 
    //         interval: 4000 
    // });

    // $('#carousel-text').carousel({ 
    //         interval: 4000 
    // });

    //Handles the carousel thumbnails
   $('[id^=carousel-selector-]').click( function(){
        var id = this.id.substr(this.id.lastIndexOf("-") + 1);
        var id = parseInt(id);  
        $('#myCarousel').carousel(id);
        $('#carousel-text').html($('#slide-content-'+id).html()); 
    }); 


    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid.bs.carousel', function (e) {
             var id = $('.item-div4.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html()); 
    });
});

// / Banner home div4 ========================================


// / Toggle Menu ANIMATION ====================================

$("#toggle-menu").click(function() {
  $(this).toggleClass("on");
  $(".navbar-nav").slideToggle();
});


// / Toggle Menu ANIMATION ====================================



  $('.count').each(function (i) {
      var bottom_of_object = $(this).position().top + $(this).outerHeight();
      var bottom_of_window = $(window).scrollTop() + $(window).height();
      /* se o objeto estiver completamente "scrollado" pra dentro da janela, fazer o fade_in*/
      if (bottom_of_window > bottom_of_object) {
        $(this).prop('Counter',0).animate({
             Counter: $(this).text()
        }, {
            duration: 5000,
            easing: 'swing',
            step: function (now) {
            $(this).text(Math.ceil(now));
            }
          });                       
      }
  });




$(window).scroll(function () {
      /* checar a localização dos elementos */
      if($(document).width() >= 992){
      $('.div-fade').each(function (i) {
          var bottom_of_object = $(this).position().top + $(this).outerHeight();
          var bottom_of_window = $(window).scrollTop() + $(window).height();
          /* se o objeto estiver completamente "scrollado" pra dentro da janela, fazer o fade_in*/
          if (bottom_of_window > bottom_of_object) {
              $(this).addClass('solucoes-div'); 
              $(this).removeClass('div-fade');                              
          }
      });
    }
  });

});


function scrollBanner() {
  var scrollPos;
  var headerText = document.querySelector('.thumb-post h1');
  scrollPos = window.scrollY;

  if (scrollPos <= 600) {
      headerText.style.transform =  "translateY(" + (-scrollPos/3) +"px" + ")";
      headerText.style.opacity = 1 - (scrollPos/600);
  }
}

window.addEventListener('scroll', scrollBanner);



$(document).ready(function() {
    $(".seta-baixo").click(function(event){
        $('html, body').animate({scrollTop: '+=700px'}, 800); 
    });
});


$(document).ready(function(){
    $(".botao1-left").click(function(){      
        $(".acender-1").css({"opacity": "1"});
        $(".apagar-botao1-left").css({"opacity": ".2","box-shadow": "0 0 0"});
        $(".bullet-cerebro").css({"box-shadow": "0 0 35px rgba(255,255,255,.8)"});
        $(this).css({"background-color": "#F3A83A", "border": "1px solid #E99114"});
        $(".botao2-left,.botao3-left,.botao1-right,.botao2-right,.botao3-right").css({"background": "none", "border": "1px solid white"});
    });
    $(".botao2-left").click(function(){      
        $(".acender-1").css({"opacity": "1"}); 
        $(".apagar-botao1-left").css({"opacity": ".2","box-shadow": "0 0 0"});
        $(".bullet-cerebro").css({"box-shadow": "0 0 35px rgba(255,255,255,.8)"});
        $(this).css({"background-color": "#F3A83A", "border": "1px solid #E99114"});
        $(".botao1-left,.botao3-left,.botao1-right,.botao2-right,.botao3-right").css({"background": "none", "border": "1px solid white"});
    });
    $(".botao3-left").click(function(){      
        $(".acender-1").css({"opacity": "1"});
        $(".apagar-botao3-left").css({"opacity": ".2","box-shadow": "0 0 0"});
        $(".bullet-cerebro").css({"box-shadow": "0 0 35px rgba(255,255,255,.8)"});
        $(this).css({"background-color": "#F3A83A", "border": "1px solid #E99114"});
        $(".botao1-left,.botao2-left,.botao1-right,.botao2-right,.botao3-right").css({"background": "none", "border": "1px solid white"});
    });
    $(".botao1-right").click(function(){      
        $(".acender-1").css({"opacity": "1"});
        $(".apagar-botao1-right").css({"opacity": ".2","box-shadow": "0 0 0"});
        $(".bullet-cerebro").css({"box-shadow": "0 0 35px rgba(255,255,255,.8)"});
        $(this).css({"background-color": "#F3A83A", "border": "1px solid #E99114"});
        $(".botao1-left,.botao2-left,.botao3-left,.botao2-right,.botao3-right").css({"background": "none", "border": "1px solid white"});
    });
    $(".botao2-right").click(function(){      
        $(".acender-1").css({"opacity": "1"});
        $(".apagar-botao1-right").css({"opacity": ".2","box-shadow": "0 0 0","cursor": "auto"});
        $(".bullet-cerebro").css({"box-shadow": "0 0 35px rgba(255,255,255,.8)","cursor": "pointer"});
        $(this).css({"background-color": "#F3A83A", "border": "1px solid #E99114"});
        $(".botao1-left,.botao2-left,.botao3-left,.botao1-right,.botao3-right").css({"background": "none", "border": "1px solid white"});
    });
    $(".botao3-right").click(function(){      
        $(".acender-1").css({"opacity": "1"});
        $(".apagar-botao3-right").css({"opacity": ".2","box-shadow": "0 0 0","cursor": "auto"});
        $(".bullet-cerebro").css({"box-shadow": "0 0 35px rgba(255,255,255,.8)","cursor": "pointer"});
        $(this).css({"background-color": "#F3A83A", "border": "1px solid #E99114"});
        $(".botao1-left,.botao2-left,.botao3-left,.botao1-right,.botao2-right").css({"background": "none", "border": "1px solid white"});
    });

    if($(document).width() >= 768){
      $(".navbar-toggle").click(function(){
        $(".navbar-collapse.collapse").css({"display": "block!important"});
      });      
    }
    

$(document).ready(function() {   
     $("#myCarousel-principal").swiperight(function() {  
        $(this).carousel('prev');  
        });   
     $("#myCarousel-principal").swipeleft(function() {  
        $(this).carousel('next');  
   });     
});  



$(document).ready(function() {   
     $("#article-photo-carousel").swiperight(function() {  
        $(this).carousel('prev');  
        });   
     $("#article-photo-carousel").swipeleft(function() {  
        $(this).carousel('next');  
   });     
});  



});
 