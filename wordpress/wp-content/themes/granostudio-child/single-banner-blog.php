<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0 
 */

get_header(); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- btn voltar -->
<!--   <div class="btnvoltar-single">
    <a href="<?php echo get_home_url(); ?>" class="btn"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
  </div> -->
<!-- /btn voltar -->

<!-- Page Content --> 
    <!-- Thumb -->

    <?php if (has_post_thumbnail()){?>
      <?php $thumbUrl = get_the_post_thumbnail_url(get_the_ID(), 'large' ); ?>
      <div class="thumb-post" style="background-image: url(<?php echo $thumbUrl; ?>)">
        <div class="mask"></div>
        <h1 class="col-sm-8 col-sm-offset-2 title_single"><?php echo get_the_title(); ?></h1>               
      </div>
      <div class="single thumb-active">
      <div class="container">
      <!-- Title -->

    <?php } else {
      ?>
      <div class="blog-single">
      <div class="container">
      <?php
    }?>
    <!-- / Thumb -->


    <div class="blog-single"> 

        <div class="row">


            <!-- Blog Post Content Column -->
            <div class="<?php echo is_active_sidebar( 'sidebar_blog' ) ? 'col-sm-8' : 'col-sm-8'; ?> coluna-single">

                <!-- Blog Post -->

                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <!-- Post Content -->
                <?php the_content(); ?>

                <hr>

                <!-- Date/Time -->
                <?php $data_atualização = get_the_modified_date(); ?>

                <p class="data"><span class="fa fa-clock-o"></span> <?php echo get_the_date(); ?> | <?php echo !empty($data_atualização) ? "Atualizado dia ".$data_atualização : ''; ?></p>
                <!-- <div class="social">
                  <p>Compartilhe</p>
                     GranoSocialShare(get_the_permalink())
                </div> -->

                <div class="fb-comments" data-href="http://sistran.tempsite.ws/" data-numposts="5" style="width: 100%;"></div>    
                <br>

                <!-- Blog Comments -->

            </div>
            <?php endwhile; // end of the loop. ?>

            <!-- Blog Sidebar Widgets Column -->
            <?php get_sidebar('blog'); ?>


        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php get_footer(); ?>
