<?php

/**
* Módulo:
* ***** Posts - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_posts(){


    ?>
    <!-- <div class="container grano-posts">
      <div class="row"> -->
        <!-- Banner -->

        <!-- / Banner -->
        <div class="col-sm-6 col-md-4 post post-linkedin">
          <a href="http://www.gessaude.com.br/grupo-linkedin/">
            <div class="post-border post-border-linkedin">
                <p style="font-size:34px;padding-bottom:0px;line-height:40px;padding-top:40px;"><i class="fa fa-linkedin fa-7x" aria-hidden="true"></i><br>Junte-se ao nosso grupo do <strong>LinkedIn</strong></p>
              <p style="padding-top:0px;line-height:22px;">Este é um grupo para debater, compartilhar experiências e trocar conhecimentos sobre Maturidade de Gestão Hospitalar que envolve: governança corporativa, estratégia empresarial, tecnologia, processos e pessoas.</p>
            </div>
          </a>
        </div>

        <?php
           $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
           $args = array( 'post_type' => 'post', 'posts_per_page' => 9, 'paged' => $paged, 'page' => $paged);
        	 $loop = new WP_Query( $args );


            if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


            <div class="col-sm-6 col-md-4 post">
              <div class="social-share <?php if (has_post_thumbnail()){ echo 'thumb';}?>">
                Compartilhe
                <?php GranoSocialShare(get_the_permalink()) ?>
              </div>

              <a href="<?php echo get_the_permalink(); ?>" class="post-link"></a>
              <div class="post-border">
                <?php if (has_post_thumbnail()){?>
                  <?php $thumbUrl = get_the_post_thumbnail_url($post_id, 'medium' ); ?>
                  <div class="img-thumb" style="background-image: url(<?php echo $thumbUrl; ?>)"></div>
                <div class="post-inner thumb">
                <?php } else {
                ?>
                <div class="post-inner">
                <?php

                };
                  $categories = get_the_category( $post_id );
                  $i=1;
                  $cats = array();
                  foreach( $categories as $category ) {
                    $cat = get_category( $category );
                    $cats = array( 'name' => $cat->name, 'slug' => $cat->slug, 'link'=> $cat->term_id );
                    $catlink = get_category_link($cats['link']);
                    if($i==count($categories)){
                      if ($cats['slug']!="sem-categoria") {
                        echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>";
                      }else{
                        echo "<a href='".$catlink."' class='categoria'></a>";
                      }
                    } else {
                      if ($cats['slug']!="sem-categoria") {
                        echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>".', ';
                      }else{
                        echo "<a href='".$catlink."' class='categoria'></a>";
                      }
                    }
                    $i++;
                  }
                  ?>

                  <h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h4>

                   <!-- Remove o jetpack do post-->
                  <?php
                  if ( function_exists( 'sharing_display' ) ) {
                    remove_filter( 'the_content', 'sharing_display', 19 );
                    remove_filter( 'the_excerpt', 'sharing_display', 19 );
                  }
                  the_excerpt(); ?>
                  <!---  --->
                    <?php  ?>
                  <!-- <a class="btn-default btn" href="<?php //echo get_the_permalink(); ?>">Leia mais</a> -->
                </div>
              </div>
            </div>
            <?php	endwhile; ?>
            <?php wp_reset_query(); ?>
      </div>


        <div class="">
          <ul class="pager">
            <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Posts Anteriores', $loop->max_num_pages); ?></li>
            <li class="lista-post"><a href="/lista-de-posts/" style="width:145px;">Lista de posts</a></li>
            <li class="next"><?php previous_posts_link( ' Próximos Posts <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
          </ul>
        </div>

      <div style="margin-bottom:35px;width:245px;margin-top:70px;">
        <a href="http://www.gessaude.com.br/videos/" style="color: #374E5A;"><h2 style="border-bottom: 3px solid #33BB9C;">CANAL GESSAÚDE</h2></a>
      </div>

      <div class="row">

        <?php $args = array( 'post_type' => 'movies', 'posts_per_page' => 3 );
          $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
              echo '<div class="col-sm-6 col-md-4 post2">';
              echo '<div class="post-border post-border2">';
              $video = get_post_meta( get_the_ID(), '_url', 1 );

              ?>
              <a href="<?php echo get_the_permalink(); ?>">
                <img src="<?php echo 'http://img.youtube.com/vi/' . substr($video, strpos($video,'?v=')+strlen('?v=')) . '/0.jpg'; ?>" width="100%">
              </a>
                <div class="post-inner2">
                  <h4>
                    <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title();?></a>
                  </h4>
                <?php the_excerpt(); ?>
                </div>

              <?php
            echo '</div>';
            echo '</div></a>';
            endwhile;
        ?>


        <a href="http://www.gessaude.com.br/videos/" class="btn btn-primmary" target="_blank" style="margin-left: 22px">Mais Vídeos</a>
      </div>

  <?php endif; ?>
  </div>


    <?php
}
 ?>
