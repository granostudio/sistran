<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div class="tituloI titulo">
  <h1>Lista de Posts</h1>
</div>

<div class="container">

<?php

   $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
   $args = array( 'post_type' => 'post', 'posts_per_page' => 15, 'paged' => $paged, 'page' => $paged);
   $loop = new WP_Query( $args );

   if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

   <a href="<?php echo get_the_permalink(); ?>"><p style="font-size: 19px;padding-bottom: 10px;padding-left: 15px;"><?php echo get_the_date( 'd/m/Y - ' ) ?></a><?php echo get_the_title(); ?></p>


  <?php endwhile; ?>
  <?php endif; ?>

  <div class="">
    <ul class="pager">
      <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Posts Anteriores', $loop->max_num_pages); ?></li>
      <li class="next"><?php previous_posts_link( ' Próximos Posts <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
    </ul>
  </div>
  </div>

<?php get_footer(); ?>
