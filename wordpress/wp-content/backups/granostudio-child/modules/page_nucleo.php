<div class="tituloI titulo">
  <h1>Núcleo de estudos de gestão de saúde</h1>
  <!-- <p class="textoT">Para estimular a discussão e o estudo de assuntos relacionados à gestão de instituições de Saúde</p> -->
</div>

<div class="nucleo-foto"></div>

<div class="container-left">
  <p class="texto">O Núcleo de Estudos de Gestão da Saúde (NEGeS) conta com seis áreas temáticas de estudo para atender às necessidades das organizações de Saúde, empresas e academia:</p>
</div>

<div class="container-right">
  <p class="texto">A ideia é integrar universidades, organizações de Saúde e empresas para que, juntas, possam desenvolver parcerias e pesquisas que resultem em conhecimento e materiais exclusivos (artigos, entrevistas, publicações etc.), contribuindo para aumentar a produtividade e a competitividade das entidades envolvidas.</p>
</div>

<div class="lista">
  <div class="linha linha1">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/nucleo-icon1.png" alt="">
    <h3>Estratégia Empresarial</h3>

  </div>
  <div class="linha linha2">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/nucleo-icon2.png" alt="">
    <h3>Governança Corporativa</h3>

  </div>
  <div class="linha linha1">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/nucleo-icon3.png" alt="">
    <h3>Tecnologias de Gestão</h3>

  </div>
  <div class="linha linha2">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/nucleo-icon4.png" alt="">
    <h3>Gerenciamento de Processos de Negócios (BPM)</h3>

  </div>
  <div class="linha linha1">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/nucleo-icon5.png" alt="">
    <h3>Gestão de Pessoas</h3>

  </div>
</div>

<div class="cadastrar">
  <p>Cadastre-se para ter acesso a conteúdos exclusivos</p>
</div>

<?php echo do_shortcode('[mc4wp_form id="282"]'); ?>

<!-- <div class="row4">
  <div class="mask"></div>
  <div class="atencao">
      <h2>Quem somos?</h2>
      <p>Moldando o futuro com as suas mãos</p>
  </div>
  <canvas></canvas>
</div> -->

<script src="<?php echo get_stylesheet_directory_uri() . '/js/network-ani.js' ?>" charset="utf-8"></script>
