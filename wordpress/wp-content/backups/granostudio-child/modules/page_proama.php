<!-- conteudo internos variavel -->
<?php
$conteudo = array();
?>


<div class="tituloI">
    <h1>PROAMA</h1>
    <p class="texto">Programa de Aceleração da Maturidade de Gestão da Saúde</p>
    <!-- <p class="texto">Programa de Aceleração da<br/>Maturidade de Gestão da Saúde</p> -->
</div>

<div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p class="p1">O Programa de Acelera&ccedil;&atilde;o da Maturidade de Gest&atilde;o da Sa&uacute;de (PROAMA), desenvolvido pela <strong>GesSa&uacute;de</strong>, &nbsp;&eacute; uma verdadeira imers&atilde;o no que h&aacute; de mais avan&ccedil;ado em metodologias de <strong>gest&atilde;o hospitalar.</strong> Criado para endere&ccedil;ar as necessidades de hospitais de pequeno e m&eacute;dio porte, tem investimento acess&iacute;vel por unir conceito de <strong>consultoria individual com workshops e treinamentos em grupo.</strong></p>
        <p class="p1">O programa tem dura&ccedil;&atilde;o de <strong>12 meses</strong> e conta com <strong>600 horas de atividades</strong>, entre <strong>workshops, capacita&ccedil;&atilde;o e mentoring. </strong>Podem participar <strong>hospitais, cl&iacute;nicas e centros de diagn&oacute;stico </strong>que<strong> n&atilde;o sejam concorrentes</strong> diretos em uma mesma regi&atilde;o. Abarca as cinco esferas necess&aacute;rias na maturidade de gest&atilde;o hospitalar: <strong>tecnologias de   gest&atilde;o, estrat&eacute;gia empresarial, governan&ccedil;a corporativa, gerenciamento de processos de neg&oacute;cios e gest&atilde;o de pessoas.</strong></p>
        <p class="p1">A principal metodologia utilizada &eacute; a de <strong>gerenciamento de resultados</strong>, tra&ccedil;ado a partir do projeto estrat&eacute;gico ou do plano empresarial da institui&ccedil;&atilde;o.</p>
        <p class="p1">Al&eacute;m de encontros per&iacute;odicos em grupo e consultoria exclusiva, o PROAMA tem um <strong>portal de conhecimento</strong>, no qual est&atilde;o dispon&iacute;veis materiais did&aacute;ticos, cursos online e divulga&ccedil;&atilde;o de eventos sobre maturidade na gest&atilde;o hospitalar.</p>
        <p style="text-align:center">
  <!-- <img src="<?php //echo get_stylesheet_directory_uri();?>/img/gessaude-cronograma.png" alt="" class="img-responsive"> -->
      </div>
      <!-- <div class="col-sm-6">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/gessaude-resultados.png" alt="" class="img-responsive">
      </div> -->
      </div>
      <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
          <div class="cronograma-link">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/proama-email2.png"><br/>
              <a href="http://www.gessaude.com.br/contato/" class="btn btn-primmary" target="_blank">Leia mais</a>
          </div>
        </div>
      </div>

</div>

<div class="div3">
  <div class="faixa"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h2>Perguntas frequentes sobre o proama, encontre aqui</h2>
      </div>
      <div class="col-sm-6 divperguntas">
        <div class="row">
          <p class="p1" id="back-to-div5">O que é Maturidade de Gestão?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div6">O que é PROAMA - Programa de Aceleração da Maturidade de Gestão de Sáude?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div8">Quem precisa do PROAMA?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div9">Quais os principais resultados esperados na participação do PROAMA?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div11">Como funciona o PROAMA?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div12">Todos os hospitais terão a mesma evolução na Maturidade de Gestão?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div14">O PROAMA se encerra em 12 meses?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div14">Como é a operacionalização do PROAMA?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div15">Quais as atividades do PROAMA?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div17">Como são realizados as atividades?</p>
        </div>
      </div>
      <div class="col-sm-6 divperguntas">
        <div class="row">
          <p class="p1" id="back-to-div18">Como as atividades se encadeiam para proporcionar a aceleração da Maturidade de Gestão?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div19">Qual o objetivo dos workshops?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div20">Como acontece a etapa de revisão/definição do Plano empresarial?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div21">Como acontece a revisão de processos?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div22">Como acontece a definição de indicadores?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div23">Qual a dinâmica de Reuniões Mensais de Avaliação de Resultados (REMAR)?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div24">Como funciona o programa de capacitação?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div25">Como funciona o mentoring de especialistas?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div26">O que são os Encontro de Gestores?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div27">Qual o cronograma típico de atividades do PROAMA?</p>
        </div>
        <div class="row">
          <p class="p1" id="back-to-div28">Ao fazer parte do PROAMA qual a garantia que o hospital tem que os resultados vão melhorar?</p>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="div4"></div>

<div class="div5">
  <div class="row2">
      <div class="coluna3">
        <div class="container">
          <div class="textoA">
            <h2>O que é maturidade de Gestão?</h2>
            <p>A maturidade de gestão é a capacidade da instituição (hospital) alcançar resultados com os recursos disponíveis.</p>
            <p>Quanto maior a capacidade de alcançar, de forma contínua e sustentável, os resultados, maior o nível de maturidade da Gestão.</p>
          </div>
        </div>
      </div>
      <div class="coluna-direita col2">
        <div class="textoE">
          <p>Em saúde o resultado está diretamente relacionado a quatro elementos:</p>
          <ul>
            <li>Segurança do paciente</li>
            <li>Qualidade do atendimento</li>
            <li>Eficiência dos processos</li>
            <li>Resultados financeiros</li>
          </ul>
        </div>
      </div>
  </div>
</div>


<div class="div6">
  <div class="faixa"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h2>O que é o PROAMA - PROGRAMA DE ACELERAÇÃO DA MATURIDADE DE GESTÃO DA SAÚDE?</h2>
      </div>
      <div class="col-sm-5 col-sm-offset-1" style="padding-top:30px;">
        <p>É um programa de gestão que pode ser aplicado de forma individual ou em forma de grupos de 2 a 5 hospitais que formarão uma parceria de resultados e trabalharão juntos para construir de
          forma evolutiva e gradativa um modelo de gestão baseado em três elementos: Estratégia, Modelo
          de Gestão e Governança Corporativa, através da implementação de ferramentas gerencias, tecnológicas
          e estratégicas e da troca de experiências entre os participantes.
        </p><br>

        <p>
          O Programa desenvolve o conceito de maturidade de gestão através de cinco pilares: Governança Corporativa,
          Estratégia Empresarial, Tecnologias, Processos e Pessoas.
        </p><br>

        <p>
          A GesSaúde acredita que todo o hospital tem sempre muito a ensinar e muito a aprender, o PROAMA reúne as instituições
          em grupos para, através da troca de experiências e aplicações de metodologias testadas e aprovadas, desenvolver sua maturidade de gestão.
        </p>
      </div>
      <div class="col-sm-6">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/div_6.png">
      </div>
    </div>
  </div>
</div>

<div class="div7"></div>

<div class="div8">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h2>Quem precisa do PROAMA?</h2>
      </div>
      <div class="col-sm-6" style="padding-top:30px;">
        <p>
          Hospitais que tenham investido em tecnologias de Gestão, passado pelo processo de informatização, não tenham
          resolvido seus problemas de Gestão, não possuam uma estratégica clara que é acompanhada de forma sistemática,
          não possuam uma governança corporativa que possibilite acompanhar a Gestão e os projetos de forma eficiente,
          não possuam processos eficientes, formalizados e bem estruturados e entenda que pode alcançar melhores resultados
          com uma melhor capacitação de suas pessoas.
        </p><br>
        <p>
          Ou simplesmente hospitais que não estejam satisfeitos com seus resultados e acreditem que evoluir seu processo
          de gestão é um caminho para melhorar estes resultados.
        </p><br>
      </div>
      <div class="col-sm-6">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/div_8.png">
      </div>
    </div>
  </div>
</div>

<div class="div9">
  <div class="container">
    <div class="faixa2"></div>
    <div class="row">
      <div class="col-sm-8">
        <h2>Quais os principais resultados esperados na participação do PROAMA?</h2>
        <p style="padding-bottom:50px; padding-top:50px;">A participação no PROAMA terá como principal resultado o aumento da Maturidade de Gestão da instituição que deverá gerar como efeitos práticos:</p>
      </div>
      <div class="col-sm-4 col-sm-offset-1">
        <p style="">Todos estes resultados devem resultar em:</p>
        <ul>
          <li>Aumento de segurança do paciente</li>
          <li>Melhoria da qualidade do atendimento</li>
          <li>Aumento da eficiência dos processos</li>
          <li>Melhoria dos resultados financeiros</li>
        </ul>
      </div>
      <div class="col-sm-6">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/div_9.png">
      </div>
    </div>
  </div>
</div>

<div class="div10"></div>

<div class="div11">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h2>Como funciona o PROAMA?</h2>
      </div>
      <div class="col-sm-8 col-sm-offset-2">
        <div class="row">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/nucleo-icon4.png">
          <p>O PROAMA é um programa de 12 meses estruturado para funcionar com grupos de 05 hospitais que, através da aplicação de metodologias
            testadas e aprovadas, vão desenvolver juntos e de forma acelerada sua maturidade de gestão.
          </p>
        </div>
        <div class="row">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/cabeca.png" class="cabeca">
          <p>O PROAMA usa como metodologia central o gerenciamento de resultados a partir do Projeto Estratégico (Plano Empresarial).</p>
        </div>
        <div class="row">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/servicos-pessoas.png" class="conexao">
          <p>É um modelo de construção conjunta para possibilitar que a instituição desenvolva as habilidades de gestão necessários a promover seu desenvolvimento corporativo.</p>
        </div>
        <div class="row">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/grafico2.png">
          <p>
            Metodologias auxiliares são implementadas em cada uma das instituições nos campos de Estratégia, Finanças, Gestão de Faturamento (SUS e Convênio), Gestão de Processos
            de Negócios, Gestão de Pessoas, Gestão de Projetos e Suprimentos, de forma a garantir o alcance dos resultados desejados de maneira intregrada e global.
          </p>
        </div>
        <div class="row">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/teclado2.png">
          <p>Com mais de 600 horas anuais atividades, as metodologias de gestão oferecidas pelo PROAMA são desenvolvidos com o objetivo de melhorar a competitividade e otimizar os resultados globais das instituições.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="div12">
  <div class="container">
    <div class="faixa2"></div>
    <div class="row">
      <div class="col-sm-8">
        <h2>Todos os hospitais terão a mesma evolução na maturidade de gestão?</h2>
      </div>
    </div>
  </div>
  <div class="container-left">
    <p>Não, a evolução da maturidade vai variar para cada instituição, pois cada uma tem uma cultura, um tempo e uma velocidade diferente para assimilar as técnicas apresentadas.</p><br>
    <p>Umas evoluirão mais em algumas áreas que outras, mas ao final de cada ciclo todas evoluirão e aumentarão seu nível de Maturidade de Gestão.</p>
  </div>
</div>

<div class="div13"></div>

<div class="div14">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-sm-offset-1">
        <h2>o proama se encerra em 12 meses?</h2>
        <p>Não, o PROAMA é um programa de 12 meses que pode ser renovado anualmente para que a instituição possa continuar evoluindo seu nível de Maturidade de Gestão e molhorando seus resultados ano a ano.</p>
      </div>
      <div class="col-sm-5 col-sm-offset-1">
        <h2>como é a operacionalização do proama?</h2>
        <p>O PROAMA é um programa com atividades definidas mês a mês onde são aplicadas as metodologias para que a instituição possa assimilar e desenvolver as habilidades necessárias a aumentar o seu nível de maturidade de Gestão.</p>
      </div>
    </div>
  </div>

</div>

<div class="div15">
  <div class="container">
    <div class="faixa2"></div>
    <div class="row">
      <div class="col-sm-8">
        <h2>Quais as atividades do PROAMA?</h2>
        <p>O PROAMA possui 7 grupos de atividades:</p>
      </div>
      <div class="col-sm-12">
        <center><img src="<?php echo get_stylesheet_directory_uri();?>/img/div_15.png"></center>
        <p>Cada grupo de atividades possui uma dinâmica específica, uma metodologia de aplicação específica e um público específico que no seu conjunto poderá envolver entre 100 e 150 profissionais por ano para desenvolver suas habilidades
         de gestão em dimensões específicas de técnicas e aplicações de Gestão.</p>
      </div>
    </div>
  </div>
</div>

<div class="div16"></div>

<div class="div17">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h2>Como são realizadas as atividades?</h2>
        <p>No PROAMA existem dois tipos de atividades:</p>
      </div>
      <div class="col-sm-6">
        <h3>Coletivas</h3>
        <p>As coletivas são atividades em que todos os hospitais participam juntos e existe uma grande troca de experiências para que, além dos conhecimentos e técnicas aprendidos pelo uso dos métodos do programa, todos possam compartilhar e aprender com os outros.</p>
        <ul>
          <li>Workshops</li>
          <li>Revisão de processos</li>
          <li>Definição de Indicadores</li>
          <li>Capacitação dos gestores</li>
          <li>Encontro de gestores</li>
        </ul>
      </div>
      <div class="col-sm-6">
        <h3>Individuais</h3>
        <p>As atividades individuais são as atividades onde são tratadas questões especificas de cada instituição, onde cada uma é única e são discutidos, acompanhados e debatidas soluções individuais para cada instituição.</p>
        <ul>
          <li>Revisão do Plano Empresarial/Planejamento estratégico</li>
          <li>Reunião de acompanhamento de resultados (REMAR)</li>
          <li>Mentoring de especialistas</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="div18">
  <div class="container">
    <div class="faixa2"></div>
    <div class="row">
      <div class="col-sm-8">
        <h2>Como as atividades se encadeiam para proporcionar a aceleração da maturidade de gestão?</h2>
      </div>
      <div class="col-sm-12">
        <p>Todas as atividades foram planejadas para desenvolver na instituição habilidades práticas que devem ser aplicadas no dia a dia e gerar resultados imediatos.</p>
        <p>O processo se inicia com um workshop de estratégia onde são apresentados os conceitos de estratégia, planejamento estratégico, orçamento empresarial e acompanhamento de indicadores de desempenho.</p>
        <p>Após o workshop acontece a revisão/definição do plano empresarial que é o momento de revisão/elaboração do planejamento estratégico, definição dos objetivos e metas estratégicas, definição dos indicadores de gestão e início da elaboração do orçamento empresarial.</p>
        <p>Após a conclusão da revisão/elaboração do plano empresarial inicia-se a fase de acompanhamento dos resultados, através do monitoramento dos indicadores definidos nas Reuniões Mensal de Acompanhamento de Resultados (Remar).</p>
        <p>A REMAR é uma reunião onde os líderes deverão mensalmente apresentar e prestar contas dos resultados de suas áreas.</p>
        <p>Mais que uma simples reunião a REMAR é um processo que implantará a cultura de monitoramento e acompanhamento de resultados em todos os níveis gerenciais e áreas da instituição.</p>
        <p>Em paralelo acontece a etapa de revisão de processos.</p>
        <p>A GesSaúde possui uma metodologia própria que possibilita em curto espaço de tempo revisar e aprimorar todos os principais processos da instituição.</p>
        <p>Após a revisão de processos de cada área acontece a definição de indicadores de Gestão da área onde serão definidos os indicadores para monitorar o processo e o resultado da área.</p>
        <p>Em paralelo ocorre também o programa de capacitação, onde até 05 gestores por instituição, passarão por um programa de treinamento que aprofundará e desenvolverá seus conhecimentos sobre diversas áreas do hospital e áreas de conhecimento indispensáveis para evoluir o processo de Gestão.</p>
        <p>Ao longo do programa existe uma agenda de visitas dos especialistas da GesSaúde para proporcionar orientações sobre melhores práticas in loco nas instituições e discutir alternativas de melhorias dos processos e dos resultados.</p>
        <p>Semestralmente acontece o Encontro de Gestores onde os principais gestores das instituições que compões os diversos grupos do PROAMA se encontrarão em um evento de relacionamento, troca de experiências e conhecimento de novos métodos de Gestão.</p>
      </div>
    </div>
  </div>
</div>

<div class="div19">
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h2>Qual o objetivo dos Workshops?</h2>
      </div>
    </div>
  </div>
  <div class="container-left">
    <p>
      Os Workshops tem o objetivo de apresentar e familiarizar os principais gestores com as ferramentas de gestão estratégica (Matriz BGC, Matriz SWOT, As cinco forças de Porter, Posicionamento Estratégico, Balance Score Card, Definição de Metas e Indicadores, Acompanhamento de Projetos, Planos de Negócios, Orçamento Empresarial, etc.) e preparar os profissionais das instituições para a etapa de revisão de processos.
    </p>
  </div>
</div>

<div class="div20">
  <div class="container-full editado">
    <div class="coluna1">
      <div class="fundo"></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-6 texto">
          <h2>Como acontece a etapa de revisão/definição do plano empresarial?</h2>
          <p class="colunaT">Atividade que será realizada de forma individual com cada instituição para revisão/definição do Plano Empresarial.</p>
          <p class="colunaT">Nesta atividade será revisto o posicionamento estratégico: Missão, Visão e Valores da instituição.</p>
          <p class="colunaT">Será realizada uma avaliação de cenários utilizando diversas ferramentas (Matriz BGG, Matriz SWOT, as cinco forças de Porter, etc.) para definição da estratégia.</p>
          <p class="colunaT">Definida a estratégia serão desdobradas as metas, projetos e inicia-se a elaboração do Orçamento Empresarial.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="div21">
  <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2>Como acontece a revisão de processos?</h2>
          <p>
            A metodologia tem o objetivo de realizar uma revisão dos principais processos das instituições, identificar oportunidades de melhoria na operação, avaliar se o modelo de operação atual atende aos requisitos desejados e projetar novos processos adequados aos objetivos de eficácia, eficiência e qualidade definidos pela Instituição.<br>
            Nesta etapa é utilizada Metodologia GesSaúde de Revisão de Processos:
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <h3>1. Workshops de revisão dos processos</h3>
          <p>É apresentado o Modelo como Referência para repensar os processos atuais;<br>
            A partir do modelo de referência serão realizadas discussões buscando definir o modelo ótimo para a instituição respeitando sua estrutura, cultura, pessoas, características e resultados esperados;<br>
            O processo é totalmente orientado a buscar os resultados desejados;
            Durante as discussões o novo processo é desenhado e repassado até que aconteçam os consensos;
          </p>
        </div>
        <div class="col-sm-6">
          <center><img src="<?php echo get_stylesheet_directory_uri();?>/img/div_21.png">
          </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <h3>2. Mapeamento da usabilidade do sistema</h3>
          <p>Nesta etapa serão avaliadas as configurações atuais do sistema de gestão e verificada a necessidade de ajustes para implementação do modelo de processos definido.<br>
            O final desta etapa é um documento que descreve todas as configurações do sistema de gestão necessárias para implementação dos processos definidos.<br></p>
            <p style="font-size:14px">*** Esta atividade deverá ser realizada por cada instituição com a orientação da equipe de consultoria.</p>
        </div>
        <div class="col-sm-6">
          <h3>3. Implantação dos processos definidos</h3>
          <p>Nesta etapa serão realizados treinamentos no processo e no sistema de gestão para qualificação dos profissionais para implementação dos processos definidos.</p>
          <p style="font-size:14px">*** Esta atividade deverá ser realizada por cada instituição com a orientação da equipe de consultoria.</p>
        </div>
      </div>
  </div>
</div>

<div class="div22">
  <div class="row2">
      <div class="coluna3">
        <div class="container">
          <div class="texto">
            <h2>Como acontece a definição de indicadores?</h2>
            <p>A metodologia tem o objetivo de apoiar na definição dos indicadores de desempenho que serão utilizados para monitorar o nível de eficiência das áreas e a execução do plano estratégico definido.</p>
            <p>Serão definidos indicadores: Estratégicos, Assistencial, Suprimentos, Faturamento SUS, Faturamento Convênios, Controladoria Financeira, Áreas de Apoio.</p>

          </div>
        </div>
      </div>
      <div class="coluna-direita col2  hidden-xs">
        <div class="fundo"></div>
      </div>
  </div>
</div>

<div class="div23">
  <div class="faixa"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <h2>Qual a dinâmica das reuniões mensais de avaliação de resultados (REMAR)?</h2>
      </div>
      <div class="col-sm-12">
        <p>As REMAR são o momento em que ocorre a avaliação dos resultados, controle de metas, e acompanhamento dos projetos para alcance dos objetivos definidos no Plano Empresarial.</p>
        <p>Reunião estratégica realizada com o corpo diretivo da instituição, tem um plano definido onde os gestores apresentam o resultado através dos indicadores de gestão de suas áreas e os projetos sob sua responsabilidade, para acompanhamento da evolução do programa, dos resultados da instituição e do Plano Empresarial.</p>
        <p>A REMAR é ápice de um processo de avaliação que começa com a avaliação dos indicadores nas áreas operacionais, passa pela gerencia e finaliza com a reunião da diretoria ou alta gestão da instituição.</p>
      </div>
    </div>
  </div>
</div>

<div class="div24">
  <div class="row2">
      <div class="coluna3">
        <div class="container">
          <div class="texto">
            <h2>Como funciona o programa de capacitação?</h2>
            <p>As atividades de mentoring são a aplicação prática dos conhecimentos de gestão realizadas pelos especialistas nas áreas de Suprimentos, Assistencial, Faturamento SUS, Faturamentos
              de Convênios, Financeira, TI e Gestão de Processos para acompanhamento e orientação quanto as melhores práticas de gestão e operação e alinhamento com o Plano Empresarial.
            </p>
          </div>
        </div>
      </div>
      <div class="coluna-direita col2  hidden-xs">
        <div class="textoD">
          <ul>
            <li>Governança Corporativa</li>
            <li>Finanças</li>
            <li>Gestão de Faturamento SUS</li>
            <li>Gestão de Faturamento Convênio</li>
            <li>Gestão de Faturamento de Negócios</li>
            <li>Gestão de Pessoas</li>
            <li>Gestão de Projetos</li>
            <li>Gestão de TI</li>
            <li>Gestão de Suprimentos</li>
            <li>Acreditação Hospitalar</li>
          </ul>
        </div>
      </div>
  </div>
</div>

<div class="div25">
  <div class="container-full editado">
    <div class="coluna1">
      <div class="fundo"></div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-6 texto">
          <h2>Como funciona o mentoring de especialistas?</h2>
          <p class="colunaT">As atividades de mentoring são a aplicação prática dos conhecimento s de gestão realizadas pelos especialistas nas áreas de Suprimentos, Assistencial, Faturamento SUS, Faturamentos de Convênios, Financeira, TI e Gestão de Processos para acompanhamento e orientação quanto as melhores práticas de gestão e operação e alinhamento com o Plano Empresarial.</p>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="div26">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <h2>O que são os encontros de gestores?</h2>
          <p>Encontro semestral dos gestores das instituições participantes do programa para discussão de temas de interesse comum.</p>
          <p>O evento tem um caráter de integrar e promover o relacionamento e troca de experiências entre os participantes do programa.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="div27">
    <div class="container">
      <div class="faixa2"></div>
      <div class="row">
        <div class="col-sm-12">
          <h2>Qual o cronograma típico de atividades do proama?</h2>
          <center><img src="<?php echo get_stylesheet_directory_uri();?>/img/gessaude-cronograma.png">
        </div>
      </div>
    </div>
  </div>

  <div class="div28">
    <div class="container">
      <div class="row">
        <div class="col-sm-10">
          <h2>Ao fazer parte do proama qual a garantia que o hospital tem que os resultados vão melhorar?</h2>
          <p>
            Hospitais que tenham investido em tecnologias de Gestão, passado pelo processo de informatização, não tenham resolvido seus problemas de Gestão, não possuam uma estratégia clara que é acompanhada de forma sistemática,
            não possuam uma governança corporativa que possibilite acompanhar a Gestão e os projetos de forma eficiente, não possuam processos eficientes, formalizados e bem estruturados e entenda que pode alcançar melhores resultados com uma melhor capacitação de suas pessoas.
          </p>
          <p>
            Ou simplesmente hospitais que não estejam satisfeitos com seus resultados e acreditem que evoluir seu processo de gestão é um caminho para melhorar estes resultados.</p>
        </div>
      </div>
    </div>
  </div>


<!-- box conteudo loop -->

<?php foreach ($conteudo as $key => $value) {
  ?>
  <div class="box-conteudo box-<?php echo $key ?>">
    <div class="btnFechar"></div>
    <?php echo $value; ?>
  </div>
  <?php
} ?>

<div class="cadastrar">
  <p>Cadastre-se para ter acesso a conteúdos exclusivos</p>
</div>

<?php echo do_shortcode('[mc4wp_form id="282"]'); ?>

<div class="interna-final">
  <div class="mask"></div>
  <div class="texto">
      <h2>Quem somos?</h2>
      <p>Moldando o futuro com as suas mãos</p>
      <a class="btn btn-primary" href="/quem-somos2/">Leia Mais</a>
  </div>
  <canvas></canvas>
</div>

<script src="<?php echo get_stylesheet_directory_uri() . '/js/network-ani.js' ?>" charset="utf-8"></script>

<script type="text/javascript">


$(document).ready(function() {

// box-conteudo ====================================

$('.leia-mais').on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  var box = $(this).data('box');
  $(box).addClass('active');
});
$('.box-conteudo .btnFechar').on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  $('.box-conteudo').removeClass('active');
});
$('.box-conteudo .btn').on('click', function(event) {
  // event.preventDefault();
  /* Act on the event */
  $('.box-conteudo').removeClass('active');
});
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
      $('.box-conteudo').removeClass('active');
    } else{

    }
});


});
</script>
