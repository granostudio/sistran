<?php get_header(); ?>

<!-- Div banner -->
<div class="banner-parcerias">
  <div class="col-sm-8 col-sm-offset-2">
    <h1><?php the_field('titulo_banner_parcerias'); ?></h1>
    <p><?php the_field('texto_banner_parcerias'); ?></p>
  </div>
</div>
<!-- Fim Div Banner -->

<!-- Div Conteúdo Parceiros -->
  <div class="container parceiros">

   <div class="row">

    <div class="col-sm-10 col-sm-offset-1">


      <?php
       $args = array( 'post_type' => 'parceiros');
       $loop = new WP_Query( $args );

       if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

      <div class="row">
        <div class="col-img <?php the_field('classe_img'); ?>">
          <?php the_post_thumbnail( ); ?>
        </div>
        <div class="col-right <?php the_field('classe_text'); ?>">
          <h2><?php echo get_the_title(); ?></h2>
          <p><?php the_content(); ?></p>
        </div>
      </div>

      <?php endwhile; // end of the loop. ?>
      <?php endif; ?>     

      <div class="row">
        <button onclick="location.href='/solucoes/'" type="button" class="botao botao-home" name="button">Veja também: Portfólio da Sistran Brasil</button>
      </div>

    </div>

   </div>
   
  </div> <!-- Fim Container -->


<!-- <div class="row"> 
        <div class="col-img col-onebase">
          <img src="<?php echo get_stylesheet_directory_uri();?>/img/onebase.png">
        </div>
        <div class="col-right col-onebase-right">
          <h2>O OnBase®  automatiza processos, gerencia todo o conteúdo empresarial em um local seguro e se integra a outros aplicativos para acesso de informações quando e onde precisar.</h2>
          <p>Permite a visibilidade do status dos seus processos, documentos e informações, além de cumprir requisitos de retenção. Possui acesso instantâneo em qualquer lugar a conteúdo e processos, até mesmo por meio de dispositivos móveis. Elimina custos de impressão, envio e armazenamento de documentos físicos com a captura eletrônica de conteúdo.</p>
        </div>
      </div> --> 

<?php get_footer(); ?>
