<?php get_header(); ?>

<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>

<!-- Div banner -->
<div class="div1">

  <div id="myCarousel-principal" class="carousel-principal slide myCarousel-principal" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li id="myCarousel-principal-0" class="active"></li>
      <li id="myCarousel-principal-1"></li>
      <li id="myCarousel-principal-2"></li>
      <li id="myCarousel-principal-3"></li> 
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">  

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'banner-home',
          'posts_per_page' => 1,
      )); 

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

      <?php while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="item active" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">            
            <div class="carousel-caption">
              <h1><?php echo get_the_title(); ?></h1>
              <p><?php the_content(); ?></p>
              <a href="/sobre/" class="botao botao-banner">Conheça a Sistran Brasil</a>
            </div>
          </div>          

      <?php endwhile; } ?>


      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'banner-home',
          'offset' => 1,
      )); 

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

      <?php while ( $query->have_posts() ) : $query->the_post(); ?>

          <div class="item" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
            <div class="carousel-caption">
              <h1><?php echo get_the_title(); ?></h1>
              <p><?php the_content(); ?></p>
              <a href="/sobre/" class="botao botao-banner">Conheça a Sistran Brasil</a>
            </div>
          </div>         

      <?php endwhile; } ?>


    </div>

<!--     <a class="left carousel-control" href="#myCarousel-principal" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel-principal" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a> -->

  </div> 

</div>
<!-- fim div1 -->

<!-- Div soluções --> 
<div class="div2">

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Soluções para o seu negócio</h1>
        <hr class="titulo">
      </div>       
    </div>

    <div class="row" style="margin-top: 50px;margin-bottom: 60px;">

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'page',
          'posts_per_page' => 1,
      )); 

      if ( $query->have_posts() ) {
      while ( $query->have_posts() ) : $query->the_post();

      // Grab the metadata from the database
      $text_icon1 = get_post_meta( get_the_ID(), 'text_icon1', true );
      $text_icon2 = get_post_meta( get_the_ID(), 'text_icon2', true );
      $text_icon3 = get_post_meta( get_the_ID(), 'text_icon3', true );
      $text_icon4 = get_post_meta( get_the_ID(), 'text_icon4', true );

      ?>

      <div class="col-sm-3 col-md-2 col-md-offset-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice6@2x.png">
        <p class="paragrafo-div"><?php echo esc_html( $text_icon1 ); ?></p>
      </div>
      <div class="col-sm-3 col-md-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice7@2x.png">
        <p class="paragrafo-div"><?php echo esc_html( $text_icon2 ); ?></p>
      </div> 
      <div class="col-sm-3 col-md-2 div-fade">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice8@2x.png">
        <p class="paragrafo-div"><?php echo esc_html( $text_icon3 ); ?></p>
      </div>
      <div class="col-sm-3 col-md-2 div-fade"> 
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice9@2x.png">
        <p class="paragrafo-div"><?php echo esc_html( $text_icon4 ); ?></p>
      </div>

      <?php endwhile; ?>

      <?php }
      wp_reset_postdata();
      ?>

    </div>

    <a href="/solucoes/"><button type="button" class="botao" name="button">Nossas soluções</button></a>
  <!-- fim div container -->
  </div>
<!-- fim div2 -->
</div>

<!-- Div destaque -->
<div class="container-fluid div3">
    <div class="row padding-secao">
      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'post',
          'posts_per_page' => 1,
          'category_name' => 'Reportagem',
      ));

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-noticias-left">

                  <ul>
                    <?php
                    foreach((get_the_category()) as $category) {
                      echo '<li class="categoria-post">' . $category->cat_name . '</li>';
                    }
                    ?>
                  </ul>
                  <a href="<?php echo esc_url( $url ); ?>"><p class="titulo-post"><?php the_title() ?></p></a>
                  <p class="excerpt-post"><?php echo the_excerpt_max_charlength(100); ?></p><br>
                  <a href="<?php echo get_the_permalink(); ?>"><button type="button" class="botao" name="button">Entenda como</button></a>

                </div>
              <?php endwhile; ?>

      <?php }
      wp_reset_postdata();
      ?>


      <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'post',
            'offset' => 1,
            'posts_per_page' => 1,
            'category_name' => 'Reportagem',
        ));

        // now check if the query has posts and if so, output their content in a banner-box div
        if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 col-md-offset-1 col-noticias-right">

                  <a href="<?php echo esc_url( $url ); ?>"><p><?php the_title() ?></p></a>
                  <p><?php echo the_excerpt_max_charlength(140); ?></p><br>
                  <a href="<?php echo get_the_permalink(); ?>"><button type="button" class="botao" name="button">Entenda como</button></a>

                </div>
              <?php endwhile; ?>

        <?php }
        wp_reset_postdata();
        ?>
    </div>
</div>
<!-- fim div3 -->

<!-- Div parceiros -->
<div class="div4">
  <div class="container">

    <div class="row" style="margin-bottom: 50px;">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Parceiros de primeira classe</h1>
        <hr class="titulo">
      </div>
    </div>

    <div id="main_area">
        <!-- Slider -->
          <div class="container" id="slider">
            <div class="row">
            <!-- Top part of the slider -->
                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5" id="carousel-bounding-box">
                  <div class="carousel slide" id="myCarousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        
                        <?php
                         $args = array( 'post_type' => 'parceiros', 'posts_per_page' => 1);
                         $loop = new WP_Query( $args );

                         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

                        <div class="active item item-div4" data-slide-number="0">
                          <?php the_post_thumbnail( ); ?>
                        </div>

                        <?php endwhile; // end of the loop. ?>
                        <?php endif; ?>   

                        <div class="item item-div4" data-slide-number="1">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png"></div>

                        <div class="item item-div4" data-slide-number="2">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png"></div>

                        <div class="item item-div4 item-onebase" data-slide-number="3">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/onebase.png"></div>                        

                      </div><!-- Carousel nav -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                  </div>
                </div>

            <div class="col-sm-6 col-md-6" id="carousel-text"></div>

              <div id="slide-content" class="slide-content col-sm-6 col-md-6" style="display: none;">

                <?php
                 $args = array( 'post_type' => 'parceiros', 'posts_per_page' => 1);
                 $loop = new WP_Query( $args );

                 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>
                
                <div id="slide-content-0">
                  <h2><?php echo get_the_title(); ?></h2>
                  <p><?php the_content(); ?></p>
                </div>

                <?php endwhile; // end of the loop. ?>
                <?php endif; ?> 


                <div id="slide-content-1">
                  <h2>Soluções de inteligência analítica para identificar fatores críticos, acompanhamento da execução de metas,</h2>
                  <p>simulações, executive mining, dashboard mining e cockpit mining integrável a mídias sociais (Facebook, Instagram, Twitter, YouTube, LinkedIn, Reclame Aqui).</p>
                </div>

                <div id="slide-content-2">
                  <h2>A Sistran Brasil é Microsoft Gold Certified Partner e trabalha</h2>
                  <p>com a fabricante para prover soluções utilizando softwares de última geração.</p>
                </div>

                <div id="slide-content-3">
                  <h2>O OnBase®  automatiza processos, gerencia todo o conteúdo empresarial em um local seguro e se integra a outros aplicativos para acesso de informações quando e onde precisar.</h2>
                  <p>Permite a visibilidade do status dos seus processos, documentos e informações, além de cumprir requisitos de retenção.</p>
                </div>                

              </div>

            </div>


          </div>


        <div class="container" id="slider-thumbs">
          <div class="row">
          <!-- Bottom switcher of slider -->
            <ul class="hide-bullets col-sm-6 col-md-4">
              <li class="col-sm-3" >
                <a class="thumbnail" id="carousel-selector-0" data-target="#carousel-example-generic" data-slide-to="0"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice1@2x.png"></a>
              </li>

              <li class="col-sm-3">
                <a class="thumbnail" id="carousel-selector-1" data-target="#carousel-example-generic" data-slide-to="1"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png"></a>
              </li>

              <li class="col-sm-3">
                <a class="thumbnail" id="carousel-selector-2" data-target="#carousel-example-generic" data-slide-to="2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png"></a>
              </li>

              <li class="col-sm-3">
                <a class="thumbnail" id="carousel-selector-3" data-target="#carousel-example-generic" data-slide-to="3"><img src="<?php echo get_stylesheet_directory_uri();?>/img/onebase.png"></a>
              </li>

            </ul>
            <ul class="col-sm-6">
              <a type="button" class="test botao" name="button" href="/parcerias/">Conheça todos nossos parceiros</a>
            </ul>
          </div>
        </div>  
    </div>


  </div> <!-- final container div 4 -->
</div>
<!-- fim div4 -->

<!-- Div blog -->
<div class="div5">

<div class="container">

  <div class="row" style="margin-top: 150px;margin-bottom: 80px;">
    <div class="col-sm-6 col-sm-offset-3">
      <h1>Blog e Imprensa</h1>
      <hr class="titulo">
    </div>
  </div>

  <div class="banner">       
  
   <div class="col-sm-12 col-md-12 col-lg-8">
    <div id="myCarousel-blog" class="carousel slide" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'banner-blog',
          'posts_per_page' => 1,
      ));

      // now check if the query has posts and if so, output their content in a banner-box div
      if ( $query->have_posts() ) { ?>

              <?php while ( $query->have_posts() ) : $query->the_post();
              $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                <div class="item active">

                  <div class="descricao-banner posts">
                    <ul class="lista-categoria">
                      <?php
                      foreach((get_the_category()) as $category) {
                        echo '<li class="hashtags">' . $category->cat_name . '</li>';
                      }
                      ?>
                    </ul>
                    <a href="<?php echo esc_url( $url ); ?>"><h2><?php the_title() ?></h2></a>
                    <p><?php echo the_excerpt_max_charlength(150); ?></p>

                    <div class="mask-post">
                       <a href="<?php echo get_the_permalink(); ?>">
                        <div class="text">
                          <ul class="lista-categoria">
                            <?php
                            foreach((get_the_category()) as $category) {
                              echo '<li class="hashtags">' . $category->cat_name . '</li>';
                            }
                            ?>
                          </ul>
                          <h3><?php echo get_the_title(); ?></h3>
                          <button type="button" class="botao" name="button">Leia Mais</button>
                        </div>
                      </a>
                    </div>

                  </div>

                  <div class="img-banner">
                    <?php the_post_thumbnail( ); ?>
                  </div>
                  <!-- <a href="<?php echo esc_url( $url ); ?>"><div class="botao"><p>Leia Mais</p></div></a> -->

                </div>
              <?php endwhile; ?>

      <?php }  ?>

        <!--  Segundo Loop -->

        <?php
        // start by setting up the query
        $query = new WP_Query( array(
            'post_type' => 'banner-blog',
            'offset' => 1,
        ));

        // now check if the query has posts and if so, output their content in a banner-box div
        if ( $query->have_posts() ) { ?>

                <?php while ( $query->have_posts() ) : $query->the_post();
                  $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                  <div class="item">

                    <div class="descricao-banner posts">
                      <ul class="lista-categoria">
                        <?php
                        foreach((get_the_category()) as $category) {
                          echo '<li class="hashtags">' . $category->cat_name . '</li>';
                        }
                        ?>
                      </ul>
                      <a href="<?php echo esc_url( $url ); ?>"><h2><?php the_title() ?></h2></a>+
                      <p><?php echo the_excerpt_max_charlength(145); ?></p>

                      <div class="mask-post">
                         <a href="<?php echo get_the_permalink(); ?>">
                          <div class="text">
                            <ul class="lista-categoria">
                              <?php
                              foreach((get_the_category()) as $category) {
                                echo '<li class="hashtags">' . $category->cat_name . '</li>';
                              }
                              ?>
                            </ul>
                            <h3><?php echo get_the_title(); ?></h3>
                            <button type="button" class="botao" name="button">Leia Mais</button>
                          </div>
                        </a>
                      </div>

                    </div>

                    <div class="img-banner">
                      <?php the_post_thumbnail( ); ?>
                    </div>
                    <!-- <a href="<?php echo esc_url( $url ); ?>"><div class="botao"><p>Leia Mais</p></div></a> -->

                  </div>
                <?php endwhile; ?>

        <?php }
        ?>

    </div>


        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel-blog" data-slide="prev" style="background-image:none">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel-blog" data-slide="next" style="background-image:none">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

  </div>

  <div class="row post posts-blog">

    <?php
     $args = array( 'post_type' => 'post', 'posts_per_page' => 4);
     $loop = new WP_Query( $args );

     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

       <div class="posts col-sm-4">
         <!-- <a href="<?php echo get_the_permalink(); ?>"> --> 
            <?php the_post_thumbnail( ); ?>
          <!-- </a> -->
            <div class="excerpt">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
              <p><?php echo the_excerpt_max_charlength(150); ?></p>
            </div>

          <div class="mask-post col-sm-4">
            <a href="<?php echo get_the_permalink(); ?>">
            <div class="text">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><?php echo get_the_title(); ?></h3>
              <button type="button" class="botao" name="button">Leia Mais</button>
            </div>
          </a>
          </div>
       </div>

   <?php endwhile; // end of the loop. ?>
   <?php endif; ?>
 
  </div>

</div>
    
</div>
<!-- fim div5 -->

<!-- Div contato -->

<div class="div6">
  <div class="container">

    <div class="row" style="margin-bottom: 50px;">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Contato</h1>
        <hr class="titulo">
      </div>
    </div>

      <?php
      // start by setting up the query
      $query = new WP_Query( array(
          'post_type' => 'page',
          'posts_per_page' => 1,
      )); 

      if ( $query->have_posts() ) {
      while ( $query->have_posts() ) : $query->the_post();

      // Grab the metadata from the database
      $text_end_sp = get_post_meta( get_the_ID(), 'text_end_sp', true );
      $text_end_comp = get_post_meta( get_the_ID(), 'text_end_comp', true );

      ?>


      <div class="row">
        <div class="col-sm-3 col-sm-offset-2">
          
          <div class="row padding">
            <h3>Sistran São Paulo:</h3>
            <p><?php echo esc_html( $text_end_sp ); ?></p>
            <p><?php echo esc_html( $text_end_comp ); ?></p>
          </div>
          
          <div class="row padding"> 
            <h3>Telefone:</h3>
            <p>+55 11 2192-4400</p>
            <?php the_field('texto_inicial_contato'); ?>
          </div>

        </div>

      <?php endwhile; ?>

      <?php }
      wp_reset_postdata();
      ?>

      <div class="col-sm-6 contato">
                    
        <?php echo do_shortcode('[contact-form-7 id="105" title="Contato"]'); ?>

      </div>

    </div>

  </div>
</div>

<!-- fim div6 -->

<?php get_footer(); ?> 
