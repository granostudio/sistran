<?php get_header(); ?>

<div class="page-contato">

  <div class="div6">
    <div class="container">

      <div class="row" style="margin-bottom: 50px;">
        <div class="col-sm-6 col-sm-offset-3">
          <h1>Contato</h1>
          <hr class="titulo">
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 div-left">
          <div class="row padding">
            <h4><?php the_field('texto_inicial_contato'); ?></h4>
          </div>

          <div class="row">
            <div class="col-left-img">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice10@2x.png">
            </div>
            <div class="col-right"> 
              <p><?php the_field('texto_primeiro_item'); ?></p>
            </div>
          </div>

          <div class="row">
            <div class="col-left-img">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice12@2x.png">
            </div>
            <div class="col-right">
              <p><?php the_field('texto_segundo_item'); ?></p>
            </div>
          </div>
 
          <div class="row">
            <div class="col-left-img">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice11@2x.png">
            </div>
            <div class="col-right">
              <p><?php the_field('texto_terceiro_item'); ?></p>
            </div>
          </div> 

          <div class="row padding">
            <h4><?php the_field('endereco_matriz'); ?></h4>
            <p style="width: 95%;"><?php the_field('descricao_endereco_matriz'); ?></p>
          </div>

          <div class="row padding">
            <h4>Telefone:</h4>
            <p><?php the_field('telefone_contato'); ?></p>
            <h4>E-mail:</h4>
            <p><?php the_field('email_contato'); ?></p>
          </div>

          <a href="/sobre/"><button type="button" class="botao botao-home" name="button">Sobre a Sistran</button></a>
        </div>

        <div class="col-sm-6 contato">
          
          <?php echo do_shortcode('[contact-form-7 id="105" title="Contato"]'); ?>

        </div>

      </div>

    </div>
  </div>

</div>

<?php get_footer(); ?>
