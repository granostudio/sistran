<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */


/**
 * Enqueues scripts and styles.
 */

 // Um custom post type chamado 'e-books'
	function wptutsplus_create_post_type() {

      // Set UI labels for Custom Post Type
      	$labels = array(
      		'name'                => _x( 'Videos', 'Post Type General Name', 'twentythirteen' ),
      		'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'twentythirteen' ),
      		'menu_name'           => __( 'Videos', 'twentythirteen' ),
      		'parent_item_colon'   => __( 'Parent Video', 'twentythirteen' ),
      		'all_items'           => __( 'Todos os videos', 'twentythirteen' ),
      		'view_item'           => __( 'View Video', 'twentythirteen' ),
      		'add_new_item'        => __( 'Adicionar novo video', 'twentythirteen' ),
      		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
      		'edit_item'           => __( 'Editar Video', 'twentythirteen' ),
      		'update_item'         => __( 'Update Video', 'twentythirteen' ),
      		'search_items'        => __( 'Procurar', 'twentythirteen' ),
      		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
      		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
      	);

      // Set other options for Custom Post Type

      	$args = array(
      		'label'               => __( 'videos', 'twentythirteen' ),
      		'description'         => __( 'Videos', 'twentythirteen' ),
      		'labels'              => $labels,
      		// Features this CPT supports in Post Editor
      		'supports'            => array( 'title', 'editor', 'thumbnail' ),
      		// You can associate this CPT with a taxonomy or custom taxonomy.
      	// 	'taxonomies'          => array( 'genres' ),
      		/* A hierarchical CPT is like Pages and can have
      		* Parent and child items. A non-hierarchical CPT
      		* is like Posts.
      		*/
      		'menu_icon'   		  => 'dashicons-video-alt3',
      		'hierarchical'        => false,
      		'public'              => true,
      		'show_ui'             => true,
      		'show_in_menu'        => true,
      		'show_in_nav_menus'   => true,
      		'show_in_admin_bar'   => true,
      		'menu_position'       => 10,
      		'can_export'          => true,
      		'has_archive'         => true,
      		'exclude_from_search' => false,
      		'publicly_queryable'  => true,
      		'capability_type'     => 'post',
      	);

      	// Registering your Custom Post Type
      	register_post_type( 'videos', $args );



		$labels = array(
	        'name' => __( 'E-Books' ),
	        'singular_name' => __( 'e-book' ),
	        'add_new' => __( 'Novo E-book' ),
	        'add_new_item' => __( 'Adicionar Novo E-Book' ),
	        'edit_item' => __( 'Editar E-Book' ),
	        'new_item' => __( 'Novo E-Book' ),
	        'view_item' => __( 'View E-Book' ),
	        'search_items' => __( 'Procurar E-Book' ),
	        'not_found' =>  __( 'E-Books não encontrados' ),
	        'not_found_in_trash' => __( 'No e-books found in Trash' ),
	    );
	    $args = array(
	        'labels' => $labels,
	        'has_archive' => true,
	        'public' => true,
	        'hierarchical' => false,
	        'capability_type'     => 'post',
          'menu_position'       => 4,
	        'menu_icon'   		  => 'dashicons-book',
	        'supports' => array(
	            'title',
	            'editor',
	            'excerpt' => false,
	            'custom-fields' => false,
	            'thumbnail',
	            'page-attributes' => false,
	        ),
	        'taxonomies' => array( 'post_tag', 'category'),
	    );
	    register_post_type( 'e-book', $args );



		$labels = array(
	        'name' => __( 'Banner-blog' ),
	        'singular_name' => __( 'banner-blog' ),
	        'add_new' => __( 'Novo banner-blog' ),
	        'add_new_item' => __( 'Adicionar Novo banner-blog' ),
	        'edit_item' => __( 'Editar banner-blog' ),
	        'new_item' => __( 'Novo banner-blog' ),
	        'view_item' => __( 'View banner-blog' ),
	        'search_items' => __( 'Search banner-blog' ),
	        'not_found' =>  __( 'No banner-blog Found' ),
	        'not_found_in_trash' => __( 'No banner-blog found in Trash' ),
	    );
	    $args = array(
	        'labels' => $labels,
	        'has_archive' => true,
	        'public' => true,
	        'hierarchical' => false,
	        'capability_type'     => 'post',
          'menu_position'       => 1,
	        'menu_icon'   		  => 'dashicons-images-alt2',
	        'supports' => array(
	            'title',
	            'editor',
	            'excerpt' => false,
	            'custom-fields' => false,
	            'thumbnail',
	            'page-attributes' => false,
	        ),
	        'taxonomies' => array( 'post_tag', 'category'),
	    );
	    register_post_type( 'banner-blog', $args );



	    $labels = array(
	        'name' => __( 'Banner-Home' ),
	        'singular_name' => __( 'banner-home' ),
	        'add_new' => __( 'Novo banner-home' ),
	        'add_new_item' => __( 'Adicionar Novo banner-home' ),
	        'edit_item' => __( 'Editar banner-home' ),
	        'new_item' => __( 'Novo banner-home' ),
	        'view_item' => __( 'View banner-home' ),
	        'search_items' => __( 'Search banner-home' ),
	        'not_found' =>  __( 'No banner-home Found' ),
	        'not_found_in_trash' => __( 'No banner-home found in Trash' ),
	    );
	    $args = array(
	        'labels' => $labels,
	        'has_archive' => true,
	        'public' => true,
	        'hierarchical' => false,
	        'capability_type'     => 'post',
          'menu_position'       => 2,
	        'menu_icon'   		  => 'dashicons-images-alt2',
	        'supports' => array(
	            'title',
	            'editor',
	            'excerpt' => false,
	            'custom-fields' => false,
	            'thumbnail',
	            'page-attributes' => false,
	        ),
	        'taxonomies' => array( 'post_tag', 'category'),
	    );
	    register_post_type( 'banner-home', $args );



	    // Set UI labels for Custom Post Type
      	$labels = array(
      		'name'                => _x( 'Parceiros', 'Post Type General Name', 'twentythirteen' ),
      		'singular_name'       => _x( 'Parceiro', 'Post Type Singular Name', 'twentythirteen' ),
      		'menu_name'           => __( 'Parceiros', 'twentythirteen' ),
      		'parent_item_colon'   => __( 'Parent Parceiro', 'twentythirteen' ),
      		'all_items'           => __( 'Todos os Parceiros', 'twentythirteen' ),
      		'view_item'           => __( 'View Parceiro', 'twentythirteen' ),
      		'add_new_item'        => __( 'Adicionar novo Parceiro', 'twentythirteen' ),
      		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
      		'edit_item'           => __( 'Editar Parceiro', 'twentythirteen' ),
      		'update_item'         => __( 'Atualizar Parceiros', 'twentythirteen' ),
      		'search_items'        => __( 'Procurar', 'twentythirteen' ),
      		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
      		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
      	);

      // Set other options for Custom Post Type

      	$args = array(
      		'label'               => __( 'Parceiros', 'twentythirteen' ),
      		'description'         => __( 'Parceiros', 'twentythirteen' ),
      		'labels'              => $labels,
      		// Features this CPT supports in Post Editor
      		'supports'            => array( 'title', 'editor', 'thumbnail' ),
      		// You can associate this CPT with a taxonomy or custom taxonomy.
      	// 	'taxonomies'          => array( 'genres' ),
      		/* A hierarchical CPT is like Pages and can have
      		* Parent and child items. A non-hierarchical CPT
      		* is like Posts.
      		*/
      		'menu_icon'   		  => 'dashicons-groups',
      		'hierarchical'        => false,
      		'public'              => true,
      		'show_ui'             => true, 
      		'show_in_menu'        => true,
      		'show_in_nav_menus'   => true,
      		'show_in_admin_bar'   => true,
      		'menu_position'       => 5,
      		'can_export'          => true,
      		'has_archive'         => true,
      		'exclude_from_search' => false,
      		'publicly_queryable'  => true,
      		'capability_type'     => 'post',
      	);

      	// Registering your Custom Post Type
      	register_post_type( 'parceiros', $args );



      	// Set UI labels for Custom Post Type
      	$labels = array(
      		'name'                => _x( 'Produtos', 'Post Type General Name', 'twentythirteen' ),
      		'singular_name'       => _x( 'Produto', 'Post Type Singular Name', 'twentythirteen' ),
      		'menu_name'           => __( 'Produtos', 'twentythirteen' ),
      		'parent_item_colon'   => __( 'Parent Produto', 'twentythirteen' ),
      		'all_items'           => __( 'Todos os Produtos', 'twentythirteen' ),
      		'view_item'           => __( 'View Produto', 'twentythirteen' ),
      		'add_new_item'        => __( 'Adicionar novo Produto', 'twentythirteen' ),
      		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
      		'edit_item'           => __( 'Editar Produto', 'twentythirteen' ),
      		'update_item'         => __( 'Atualizar Produtos', 'twentythirteen' ),
      		'search_items'        => __( 'Procurar', 'twentythirteen' ),
      		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
      		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
      	);

      // Set other options for Custom Post Type

      	$args = array(
      		'label'               => __( 'Produtos', 'twentythirteen' ),
      		'description'         => __( 'Produtos', 'twentythirteen' ),
      		'labels'              => $labels,
      		// Features this CPT supports in Post Editor
      		'supports'            => array( 'title', 'editor', 'thumbnail' ),
      		// You can associate this CPT with a taxonomy or custom taxonomy.
      	// 	'taxonomies'          => array( 'genres' ),
      		/* A hierarchical CPT is like Pages and can have
      		* Parent and child items. A non-hierarchical CPT
      		* is like Posts.
      		*/
      		'menu_icon'   		  => 'dashicons-cart',
      		'hierarchical'        => false,
      		'public'              => true,
      		'show_ui'             => true, 
      		'show_in_menu'        => true,
      		'show_in_nav_menus'   => true,
      		'show_in_admin_bar'   => true,
      		'menu_position'       => 6,
      		'can_export'          => true,
      		'has_archive'         => true,
      		'exclude_from_search' => false,
      		'publicly_queryable'  => true,
      		'capability_type'     => 'post',
      	);

      	// Registering your Custom Post Type
      	register_post_type( 'produtos', $args );



        // Set UI labels for Custom Post Type
        $labels = array(
          'name'                => _x( 'Bullets Points Cérebro', 'Post Type General Name', 'twentythirteen' ),
          'singular_name'       => _x( 'Bullet Point Cérebro', 'Post Type Singular Name', 'twentythirteen' ),
          'menu_name'           => __( 'Cérebro - Bullets Points', 'twentythirteen' ),
          'parent_item_colon'   => __( 'Parent Bullet Point', 'twentythirteen' ),
          'all_items'           => __( 'Todos os Bullets Points', 'twentythirteen' ),
          'view_item'           => __( 'View Bullet Point', 'twentythirteen' ),
          'add_new_item'        => __( 'Adicionar novo Bullet Point', 'twentythirteen' ),
          'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
          'edit_item'           => __( 'Editar Bullet Point', 'twentythirteen' ),
          'update_item'         => __( 'Atualizar Bullet Point', 'twentythirteen' ),
          'search_items'        => __( 'Procurar', 'twentythirteen' ),
          'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
          'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
        );

      // Set other options for Custom Post Type

        $args = array(
          'label'               => __( 'Cérebro - Bullets Points', 'twentythirteen' ),
          'description'         => __( 'Bullets Points referentes ao Cérebro', 'twentythirteen' ),
          'labels'              => $labels,
          // Features this CPT supports in Post Editor
          'supports'            => array( 'title', 'editor', 'thumbnail' ),
          // You can associate this CPT with a taxonomy or custom taxonomy.
        //  'taxonomies'          => array( 'genres' ),
          /* A hierarchical CPT is like Pages and can have
          * Parent and child items. A non-hierarchical CPT
          * is like Posts.
          */ 
          'menu_icon'           => '/wp-content/themes/granostudio-child/img/brain-3.png',
          'hierarchical'        => false,
          'public'              => true,
          'show_ui'             => true,  
          'show_in_menu'        => true,
          'show_in_nav_menus'   => true,
          'show_in_admin_bar'   => true,
          'menu_position'       => 3,
          'can_export'          => true,
          'has_archive'         => true,
          'exclude_from_search' => false,
          'publicly_queryable'  => true,
          'capability_type'     => 'post',
        );

        // Registering your Custom Post Type
        register_post_type( 'bullet-cerebro', $args );



	}
	add_action( 'init', 'wptutsplus_create_post_type' );



add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );


//Configurações e criação de metabox.
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Opções', 'cmb2' ),
        'object_types'  => array( 'videos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    // URL text field
    $cmb->add_field( array(
        'name' => __( 'URL do vídeo', 'cmb2' ),
        'desc' => __( 'Digite a URL do vídeo', 'cmb2' ),
        'id'   => '_url',
        'type' => 'text_url',
    ) );



    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Opções', 'cmb2' ),
        'object_types'  => array( 'banner', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    // URL text field
    $cmb->add_field( array(
        'name' => __( 'URL do vídeo', 'cmb2' ),
        'desc' => __( 'Digite a URL do vídeo', 'cmb2' ),
        'id'   => '_url',
        'type' => 'text_url',
    ) );


	// Start with an underscore to hide fields from custom fields list
    // $prefix = 'destaque';

    $cmb = new_cmb2_box( array(
      'id'            => 'Destaques',
      'title'         => __( 'Opções', 'cmb2' ),
      'object_types'  => array( 'destaque', ), // Post type
      'context'       => 'normal',
      'priority'      => 'high',
      'show_names'    => true,
      ) );

	// URL text field
	$cmb->add_field( array(
			'name' => __( 'Conteúdo do botão', 'cmb2' ),
			'desc' => __( 'Texto dentro do botão', 'cmb2' ),
			'id'   => '_url',
			'type' => 'text_url',
	) );


		// Start with an underscore to hide fields from custom fields list

	 $cmb = new_cmb2_box( array(
		 'id'            => 'Banner-blog',
		 'title'         => __( 'Opções', 'cmb2' ),
		 'object_types'  => array( 'banner-blog', ), // Post type
		 'context'       => 'normal',
		 'priority'      => 'high',
		 'show_names'    => true,
		 ) );

	 // URL text field
   $cmb->add_field( array(
       'name' => __( 'Link com conteúdo do site', 'cmb2' ),
       'desc' => __( 'Digite o link', 'cmb2' ),
       'id'   => '_url',
       'type' => 'text_url',
   ) );



   $cmb = new_cmb2_box( array(
		 'id'            => 'banner-home',
		 'title'         => __( 'Opções', 'cmb2' ),
		 'object_types'  => array( 'banner-blog', ), // Post type
		 'context'       => 'normal',
		 'priority'      => 'high',
		 'show_names'    => true,
		 ) );

// URL text field
   $cmb->add_field( array(
       'name' => __( 'Link com conteúdo do site', 'cmb2' ),
       'desc' => __( 'Digite o link', 'cmb2' ),
       'id'   => '_url',
       'type' => 'text_url',
   ) );



   $cmb = new_cmb2_box( array(
    'id'           => 'contact-information', 
    'title'        => 'Campos editáveis página Home',
    'object_types' => array( 'page' ), // post type
    'show_on'      => array( 'key' => 'id', 'value' => array( 231 ) ),
    'context'      => 'normal', //  'normal', 'advanced', or 'side'
    'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
    'show_names'   => true, // Show field names on the left
  ) );

    $cmb->add_field( array( 
        'name'    => 'Ícone 1 - Soluções para o seu negócio',
        'desc'    => 'Campo referente ao texto que aparece logo após o primeiro ícone',
        'default' => '',
        'id'      => 'text_icon1',
        'type'    => 'text',
    ) );

    $cmb->add_field( array( 
        'name'    => 'Ícone 2 - Soluções para o seu negócio',
        'desc'    => 'Campo referente ao texto que aparece logo após o segundo ícone',
        'default' => '',
        'id'      => 'text_icon2',
        'type'    => 'text',
    ) );

    $cmb->add_field( array( 
        'name'    => 'Ícone 3 - Soluções para o seu negócio',
        'desc'    => 'Campo referente ao texto que aparece logo após o terceiro ícone',
        'default' => '',
        'id'      => 'text_icon3',
        'type'    => 'text',
    ) );

    $cmb->add_field( array( 
        'name'    => 'Ícone 4 - Soluções para o seu negócio',
        'desc'    => 'Campo referente ao texto que aparece logo após o quarto ícone',
        'default' => '',
        'id'      => 'text_icon4',
        'type'    => 'text',
    ) );

    $cmb->add_field( array( 
        'name'    => 'Ícone 4 - Soluções para o seu negócio',
        'desc'    => 'Campo referente ao texto que aparece logo após o quarto ícone',
        'default' => '',
        'id'      => 'text_icon4',
        'type'    => 'text',
    ) );

    $cmb->add_field( array( 
        'name'    => 'Endereço - Sistran São Paulo',
        'desc'    => 'Campo referente ao endereço da Sistran São Paulo',
        'default' => '',
        'id'      => 'text_end_sp',
        'type'    => 'text',
    ) );

    $cmb->add_field( array( 
        'name'    => 'Complemento de Endereço - Sistran São Paulo',
        'desc'    => 'Campo referente ao complemento de endereço da Sistran São Paulo',
        'default' => '',
        'id'      => 'text_end_comp',
        'type'    => 'text',
    ) );
}

add_theme_support( 'post-thumbnails' );


// offset the main query on the home page
function tutsplus_offset_main_query ( $query ) {
     if ( $query->is_home() && $query->is_main_query() ) {
         $query->set( 'offset', '1' );
    }
 }


 //  O número 80 é a quantidade de caracteres a exibir.
function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
	}


function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}	 


function granostudio_scripts_child() {

 //Desabilitar jquery
 wp_deregister_script( 'jquery' );

 // Theme stylesheet.
 wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
 // import fonts (Google Fonts)
 wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
 // Theme front-end stylesheet
 wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

 // scripts js
 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);

 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js');

 // network effet home

}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
